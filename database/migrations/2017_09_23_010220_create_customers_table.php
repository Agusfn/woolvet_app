<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('customers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("id_customer_tnube");
            $table->string("nombre");
            $table->string("email");
            $table->string("telefono");
            $table->string("direccion")->default("");
            $table->string("localidad")->default("");
            $table->string("ciudad")->default("");
            $table->string("codigo_postal")->default("");
            $table->string("provincia")->default("");
            $table->string("pais")->default("");
            $table->float("total_gastado");
            $table->datetime("fecha_registro");
            $table->boolean("activo");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
}
