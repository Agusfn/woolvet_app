<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("id_producto_tnube");
            $table->integer("user_id");
            $table->string("nombre");
            $table->string("url_tienda");
            $table->string("img_ilustrativa", 400);
            $table->integer("variantes");
            $table->string("categoria");
            $table->boolean("publicado");
            $table->boolean("eliminado");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
