<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductVariantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_variants', function (Blueprint $table){
            $table->increments('id');
            $table->integer("product_id");
            $table->integer("id_variante_tnube");
            $table->integer("id_producto_tnube");
            $table->float("precio");
            $table->float("precio_promocional");
            $table->integer("stock");
            $table->string("variante_1");
            $table->string("variante_2");
            $table->string("variante_3");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_variants');
    }
}
