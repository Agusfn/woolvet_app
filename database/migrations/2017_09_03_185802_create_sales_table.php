<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales', function (Blueprint $table) {

            $table->increments('id');
            $table->integer("product_id");
            $table->integer("variant_id");
            $table->integer("user_id");
            $table->integer("customer_id");
            $table->integer("id_orden_tnube");
            $table->integer("nro_orden_tnube");
            $table->integer("id_cliente_tnube");
            $table->integer("id_producto_tnube");
            $table->integer("id_variante_tnube");
            $table->dateTime("fecha_concretada");
            $table->string("nombre_producto");
            $table->float("precio_venta", 8, 2);
            $table->integer("cantidad");
            $table->string("forma_pago");
            $table->string("forma_envio");
            $table->float("costo_envio_comprador");
            $table->float("costo_envio");
            $table->string("localidad_envio");
            $table->string("ciudad_envio");
            $table->string("cod_postal_envio");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ventas');
    }
}
