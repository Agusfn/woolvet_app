<?php

use Illuminate\Database\Seeder;
use App\User;

class SeedAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	"role" => "superadmin",
        	"confirm_status" => "",
        	"name" => "Agustin",
        	"email" => "agusfn20@gmail.com",
        	"password" => bcrypt("20596")
        ]);
    }
}
