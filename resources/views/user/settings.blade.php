@section('title', 'Mi cuenta')

@extends('layouts.app')

@section('content')

<div class="container">

	<div class="row">

		<div class="col-md-8 col-md-offset-2">

			<div class="panel panel-default">
				<div class="panel-heading">Configuración de cuenta</div>
				<div class="panel-body form-horizontal" style="padding: 20px">
					

					@if(Auth::user()->isAdmin())
						<div class="form-group">
							<label class="col-md-5" style="text-align: right;">Nombre :</label>
					        <div class="col-md-5">{{ $user->name }}</div>
						</div>
					@else
						<div class="form-group">
							<label class="col-md-5" style="text-align: right;">Nombre de marca:</label>
					        <div class="col-md-5">{{ $user->name }}</div>
						</div>

						<div class="form-group">
							<label class="col-md-5" style="text-align: right;">Nombre de responsable:</label>
					        <div class="col-md-5">{{ $user->nombre_responsable }}</div>
						</div>
					@endif


					<div class="form-group">
						<label class="col-md-5" style="text-align: right;">E-mail:</label>
				        <div class="col-md-5">{{ $user->email }}</div>
					</div>

					<div style="text-align: right">
						<a href="{{ URL::to('cuenta/password') }}" class="btn btn-primary">Cambiar contraseña</a>
					</div>

				</div>
			</div>


		</div>

	</div>

</div>

@endsection

