@section('title', 'Confirmación pendiente')

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body" style="text-align: center;">
                    <span style="font-size:18px;">Tu cuenta de marca aún no ha sido confirmada para operar.</span><br/>
                    Si luego de 72hs no han habido cambios, envíanos un e-mail a {{ config("mail.brand_support_email")  }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
