@section('title', 'Informar problema o sugerencia')

@extends('layouts.app')

@section('content')

<div class="container">

	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="panel panel-default">

				<div class="panel-heading"><h3 class="panel-title">Hacer sugerencia / informar un problema</h3></div>

				<div class="panel-body" style="padding: 30px;">

					@if(Session::has('success'))
						<div class="alert alert-success">El mensaje ha sido enviado correctamente, lo revisaremos cuanto antes. Gracias!</div>
					@endif

					{{ Form::open( array("url" => "reportar", "method" => "post") ) }}

						<div class="form-group">

							{{ Form::label('message_type', 'Tipo de mensaje') }}

							{{ Form::select('message_type', [
								'bug' => 'Informar un problema', 
								'sugerencia' => 'Hacer una sugerencia'
							], null, array('class' => 'form-control') ) }}


						</div>

						<div class="form-group">

							{{ Form::label('mensaje', 'Texto del mensaje') }}
							
							{{ Form::textarea('mensaje', null, array('class' => 'form-control') ) }}

						</div>

						@include ("layouts.errors")

						<div class="form-group" style="text-align: right">
							{{ Form::submit('Enviar', array('class' => 'btn btn-primary')) }}
						</div>

					{{ Form::close() }}

				</div>

			</div>
		</div>
	</div>

</div>

@endsection

