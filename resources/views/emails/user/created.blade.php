@component('mail::message')

Estimado/a {{ $user->nombre_responsable }}, tu cuenta de marca {{ $user->name }} ha sido registrada correctamente en Woolvet.

El siguiente paso es verificar tu dirección e-mail haciendo click en el siguiente enlace:

@component('mail::button', ['url' => URL::to('cuenta/verificar').'/'.$user->verified_email_token])
Verificar dirección
@endcomponent

Una vez verificado el e-mail, deberás esperar nuestra confirmación para comenzar a operar en Woolvet, te lo vamos a notificar por e-mail.

Si no recibís nuestra confirmación después de 48hs, contactate con nosotros: {{ config('mail.brand_support_email') }}


Un saludo,<br>
{{ config('app.name') }}
@endcomponent
