@component('mail::message')

Estimado/a {{ $user->nombre_responsable }}, tu cuenta de marca {{ $user->name }} ha sido confirmada para vender en Woolvet.

Ahora podés iniciar sesión en tu <a href="{{ config('mail.brand_panel_url') }}">panel de administración</a>, donde podrás ver en tiempo real tus ventas, ver los productos en venta, su stock, y más!

Para cualquier gestión de productos recordá que podés contactarnos a <strong>{{ config('mail.brand_support_email') }}</strong> o por medio de nuestro representante con quien hayas tramitado el ingreso.

Un saludo,<br>
{{ config('app.name') }}
@endcomponent
