@component('mail::message')

El usuario {{ $user->nombre_responsable }}, de la marca {{ $user->name }} envió un mensaje de notificación de {{ $messageType }}:


"
{{ $message }}
"

@endcomponent
