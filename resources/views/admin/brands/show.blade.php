@section('title', $user->name." - Marcas")

@extends('layouts.app')

@section('content')

	<div class="container">


	    <ol class="breadcrumb panel">
	        <li><a href="{{ URL::to('admin') }}">Panel admin</a></li>
	        <li><a href="{{ URL::to('admin/marcas') }}">Marcas</a></li>
	        <li class="active">{{ $user->name }}</li>
	    </ol>

		<div class="clearfix" style="margin-bottom:20px">

			<div class="btn-group" style="float: right;">
				<button type="button" class="btn btn-primary dropdown-toggle" style="margin-top:-9px" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			    	Acciones <span class="caret"></span>
			  	</button>
				<ul class="dropdown-menu">
					@if ($user->isUnconfirmedBrand() && Auth::user()->isSuperAdmin())
				    	<li><a href='{{ URL::to("admin/admins/setadmin/".$user->id) }}'>Hacer admin</a></li>
				    @endif
				</ul>
			</div>
		</div>



		<div class="panel panel-default">

			<div class="panel-heading">
				<h3 class="panel-title">Datos de la marca</h3>
			</div>

			<div class="panel-body">


				<div class="row">
					
					<div class="col-md-2"><label>ID:</label>&nbsp;&nbsp;<span>{{ $user->id }}</span></div>
					
					<div class="col-md-4"><label>Nombre:</label>&nbsp;&nbsp;<span>{{ $user->name }}</span></div>

					<div class="col-md-4"><label>Fecha registrada:</label>&nbsp;&nbsp;<span>{{ $user->created_at->format('d/m/Y') }}</span></div>
					

					<div class="col-md-2">
						@if($user->isConfirmedBrand())
							<span class="label label-success" style="font-size: 12px;">Confirmada</span>
						@elseif($user->isUnconfirmedBrand())
							<span class="label label-primary" style="font-size: 12px;">Pendiente confirmación</span>
						@else
							<span class="label label-danger" style="font-size: 12px;">Rechazada</span>
						@endif
					</div>


				</div>

				<div class="row" style="margin-top:50px">


					<div class="col-md-6"><label>E-mail:</label><br/><span><a href="mailto:{{ $user->email }}">{{ $user->email }}</a></span></div>
					<div class="col-md-6"><label>Nombre responsable:</label><br/><span>{{ $user->nombre_responsable }}</span></div>
					


				</div>

			</div>
		</div>

		@if($user->isConfirmedBrand())
			<div class="panel panel-default">

				<div class="panel-heading">
					<h3 class="panel-title">Productos</h3>
				</div>

				<div class="panel-body">


				</div>
			</div>

			<div class="panel panel-default">

				<div class="panel-heading">
					<h3 class="panel-title">Ventas</h3>
				</div>

				<div class="panel-body">


				</div>
			</div>
		@endif



	</div>

@endsection


@section("additional_style")
	<style type="text/css">	
		.panel .panel-body
		{
			padding:40px 30px;
		}

		.panel span
		{
			font-size:16px;
		}
	</style>


@endsection