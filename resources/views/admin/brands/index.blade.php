@section('title', 'Marcas')

@extends('layouts.app')

@section('content')

<div class="container">

    <ol class="breadcrumb panel">
        <li><a href="{{ URL::to('admin') }}">Panel admin</a></li>
        <li class="active">Marcas</li>
    </ol>



    <h3 style="text-align: center; margin: 20px 0 20px 0;" >Marcas</h3>

    @include("layouts.errors")

    @include("admin.brands.layouts.brand-confirmation-list")

    <div class="form-group clearfix">
        <div style="float:right;">
            Ordenar por:
            <select class="form-control" id="sort-brands-input">
                <option value="recientes">Más recientes</option>
                <option value="antiguas">Más antiguas</option>
                <option value="nombre_asc">Nombre (A-Z)</option>
                <option value="nombre_desc">Nombre (Z-A)</option>
                <!--option value="productos_desc">Cant. productos (mayor)</option>
                <option value="productos_asc">Cant. productos (menor)</option-->
            </select>
        </div>
    </div>

    <table class="table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Productos</th>
                <th>E-mail</th>
                <th>Fecha registro</th>
                <th>ID</th>
            </tr>
        </thead>
        <tbody>
            
            @foreach($brands as $brand)

                <tr>
                    <td><a href="{{ URL::to('admin/marcas/' . $brand->id) }}">{{ $brand["name"] }}</a></td>
                    <td>{{ $brand->products->count() }}</td>
                    <td>{{ $brand["email"] }}</td>
                    <td>{{ date("d/m/Y", strtotime($brand["created_at"])) }}</td>
                    <td>{{ $brand["id"] }}</td>
                </tr>

            @endforeach

        </tbody>

    </table>

    <div style="text-align: center;">
        {{ $brands->links() }}
    </div>




</div>

@endsection


@section("additional_scripts")

    <script type="text/javascript">
        $(document).ready(function() {

            @if(request("ordenar") !== null)
                $("#sort-brands-input").val("{{ htmlspecialchars(request('ordenar')) }}");
            @endif

            $("#sort-brands-input").change(function() {
                window.location.href = location.protocol + '//' + location.host + location.pathname + "?ordenar=" + $(this).val();
            });

        });
    </script>

@endsection