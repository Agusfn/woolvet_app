@if(count($unconfirmedBrands))

    <h4 style="text-align: center; margin:40px 0 20px 0;">Marcas pendientes de confirmación</h3>

    <table class="table">
        <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <th>Nombre</th>
                <th>E-mail</th>
                <th>Fecha registro</th>
                <th>Acción</th>
            </tr>
        </thead>
        <tbody>
            
            @foreach($unconfirmedBrands as $brand)

                <tr>
                    <td><a href="{{ URL::to('admin/marcas/' . $brand->id) }}">Ver</a></td>
                    <td>{{ $brand["id"] }}</td>
                    <td>{{ $brand["name"] }}</td>
                    <td>{{ $brand["email"] }}</td>
                    <td>{{ date("d/m/Y", strtotime($brand["created_at"])) }}</td>
                    <td>
                        {{ Form::open( array("url" => "admin/marcas/confirmar/".$brand["id"], "method" => "post", "style" => "display:inline-block") ) }}
                            {{ Form::hidden('confirm_status', 'confirmed') }}
                            <button type="submit" class="btn btn-success" data-toggle="tooltip" data-placement="top" title="Confirmar ingreso"><span class="glyphicon glyphicon-ok"></span></button>
                        {{ Form::close() }}

                        {{ Form::open( array("url" => "admin/marcas/confirmar/".$brand["id"], "method" => "post", "style" => "display:inline-block") ) }}
                            {{ Form::hidden('confirm_status', 'rejected') }}
                            <button type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title="Rechazar ingreso"><span class="glyphicon glyphicon-remove"></span></button>
                        {{ Form::close() }}
                    </td>
                </tr>

            @endforeach

        </tbody>

    </table>

    <hr style="margin:40px 0">

@endif