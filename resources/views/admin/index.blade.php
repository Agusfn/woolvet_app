@section('title', 'Panel admin')

@extends('layouts.app')

@section('content')

    <div class="container">

	    <ol class="breadcrumb panel">
	        <li class="active">Panel admin</li>
	    </ol>

    	<div class="row">

    		<div class="col-md-7">
    			
    			<div class="panel panel-default">
					<div class="panel-heading"><h3 class="panel-title">Inicio</h3></div>
					<div class="panel-body" style="height: 321px">

						Acá aparece la información importante

					</div>
				</div>

    			<div class="panel panel-default">
					<div class="panel-heading"><h3 class="panel-title">Ultimas ventas</h3></div>
					<div class="panel-body">

						Estadisticas

					</div>
				</div>


    		</div>

    		<div class="col-md-5">
    			
    			<div class="panel panel-default">
					<div class="panel-heading"><h3 class="panel-title">Administración</h3></div>
					<div class="panel-body">

			            <a href="{{ URL::to('admin/marcas') }}" class="btn btn-primary btn-block">Administrar marcas</a>

						<a href="{{ URL::to('admin/productos') }}" class="btn btn-primary btn-block">Ver productos</a>

						<a href="{{ URL::to('admin/ventas') }}" class="btn btn-primary btn-block">Ver ventas</a>

						<a href="{{ URL::to('admin/clientes') }}" class="btn btn-primary btn-block">Ver clientes</a>

					</div>
				</div>

				<div class="panel panel-default">
					<div class="panel-heading"><h3 class="panel-title">Admin. interna</h3></div>
					<div class="panel-body">

						<a href="{{ URL::to('admin/webhooks') }}" class="btn btn-primary btn-block">Administrar webhooks</a>

			            @if (Auth::user()->isSuperAdmin())
			            	<a href="{{ URL::to('admin/admins') }}" class="btn btn-primary btn-block">Administrar admins</a>
			            @endif

					</div>
				</div>

    		</div>

    	</div>




    </div>

@endsection
