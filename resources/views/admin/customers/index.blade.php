@section('title', 'Clientes')

@extends('layouts.app')

@section('content')

<div class="container">

    <ol class="breadcrumb panel">
        <li><a href="{{ URL::to('admin') }}">Panel admin</a></li>
        <li class="active">Clientes</li>
    </ol>

    <h3 style="text-align: center;">Clientes</h3>

    <div class="row" style="margin: 30px 0;">
        <div class="col-sm-2"><a href="{{ URL::to('admin/clientes/actualizar') }}" class="btn btn-primary">Revisar clientes nuevos</a></div>
        <div class="col-sm-8"></div>
        <div class="col-sm-2"><a href="{{ config('app.tnube_adminpanel_url').'/customers' }}" target="_blank" class="btn btn-success" style="float: right; margin-top:-5px">Ver en Tiendanube</a></div>
    </div>


        @if(session("message") !== null)

            <div class="alert alert-warning alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                {{ session("message") }}
            </div>

        @endif

    <table class="table">
        <thead>
            <tr>
                <th>Email</th>
                <th>Nombre</th>
                <th>Localidad</th>
                <th>Provincia</th>
                <th>Total gastado</th>
                <th>Activo</th>
                <th>ID Tnube</th>
            </tr>
        </thead>
        <tbody>
            
            @foreach($customers as $customer)

                <tr>
                    <td><a href="{{ URL::to('admin/clientes/' . $customer->id) }}">{{ $customer->email }}</a></td>
                    <td>{{ $customer->nombre }}</td>
                    <td>{{ $customer->localidad }}</td>
                    <td>{{ $customer->provincia }}</td>
                    <td>${{ $customer->total_gastado }}</td>
                    <td>
                        @if($customer->activo)
                            Si
                        @else
                            No
                        @endif
                    </td>
                    <td><a href="{{ config('app.tnube_adminpanel_url').'/customers/'.$customer->id_customer_tnube }}" target="_blank">{{ $customer->id_customer_tnube }}</a></td>
                </tr>

            @endforeach

        </tbody>

    </table>

</div>

@endsection


