@section('title', 'Detalles cliente')

@extends('layouts.app')

@section('content')

<div class="container">

    <ol class="breadcrumb panel">
        <li><a href="{{ URL::to('admin') }}">Panel admin</a></li>
        <li><a href="{{ URL::to('admin/clientes') }}">Clientes</a></li>
        <li class="active">{{ $customer->id }}</li>
    </ol>


    <div class="panel panel-default">

        <div class="panel-heading">
            <h3 class="panel-title">Datos personales</h3>
        </div>

        <div class="panel-body">



            <div class="row">

                <div class="col-sm-3">
                    <strong>ID:</strong> 
                    <span>{{ $customer->id }}</span>
                </div>   

                <div class="col-sm-3">
                    <strong>ID Tnube:</strong>
                    <span><a href="{{ config('app.tnube_adminpanel_url').'/customers/'.$customer->id_customer_tnube }}" target="_blank">{{ $customer->id_customer_tnube }}</a></span>
                </div>

                <div class="col-sm-3">
                    <strong>Fecha registro:</strong> 
                    <span>{{ $customer->fecha_registro->format('d/m/Y') }}</span>
                </div>

                <div class="col-sm-3">
                    @if($customer->activo)
                        <span class="label label-success" style="font-size:14px;">Cuenta activa</span>
                    @else
                        <span class="label label-default" style="font-size:14px;">Cuenta inactiva</span>
                    @endif
                </div>

            </div>

            <div class="row" style="margin-top:30px">

                <div class="col-sm-3"><strong>Nombre:</strong> <span>{{ $customer->nombre }}</span></div>        
                <div class="col-sm-3"><strong>Email:</strong> <span>{{ $customer->email }}</span></div>
                <div class="col-sm-3"><strong>Teléfono:</strong> <span>{{ $customer->telefono }}</span></div>

                
            </div>


        </div>
    </div>


    <div class="panel panel-default">

        <div class="panel-heading">
            <h3 class="panel-title">Dirección</h3>
        </div>

        <div class="panel-body">

            <div class="row">
                <div class="col-sm-4"><strong>Dirección:</strong> <span>{{ $customer->direccion }}</span></div>        
                <div class="col-sm-4"><strong>Localidad:</strong> <span>{{ $customer->localidad }}</span></div>
                <div class="col-sm-4"><strong>Ciudad:</strong> <span>{{ $customer->ciudad }}</span></div>
            </div>

            <div class="row" style="margin-top:30px">
                <div class="col-sm-4"><strong>Cód. postal:</strong> <span>{{ $customer->codigo_postal }}</span></div>        
                <div class="col-sm-4"><strong>Provincia:</strong> <span>{{ $customer->provincia }}</span></div>
                <div class="col-sm-4"><strong>País:</strong> <span>{{ $customer->pais }}</span></div>  
            </div>

        </div>
    </div>

    <div class="panel panel-default">

        <div class="panel-heading">
            <h3 class="panel-title">Compras</h3>
        </div>

        <div class="panel-body">

            <div class="row" style="margin-bottom: 30px;">
                <div class="col-sm-12"><strong>Total gastado:</strong> <span>${{ $customer->total_gastado }} ARS</span></div>
            </div>



        </div>
    </div>


</div>

@endsection


@section("additional_style")

    <style type="text/css">
        
        .panel .panel-body
        {
            padding:30px;
        }

        .panel span
        {
            font-size:15px;
        }

    </style>

@endsection