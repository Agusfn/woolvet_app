@section('title', 'Editar webhook')

@extends('layouts.app')


@section('content')


<div class="container">
	
	<div class="form-group clearfix">

	    <ol class="breadcrumb panel">
	        <li><a href="{{ URL::to('admin') }}">Panel admin</a></li>
	        <li><a href="{{ URL::to('admin/webhooks') }}">Webhooks</a></li>
	        <li class="active">Editar</li>
	    </ol>

		{{ Form::open( array("route" => array("admin.webhooks.destroy", $webhook->id), "method" => "DELETE", "style" => "float:right;") ) }}

			{{ csrf_field() }}
			<div class="form-group" style="text-align: center">
				<input type="button" class="btn btn-danger" onclick="if(confirm('Eliminar webhook?')) $(this).closest('form').submit();" value="Eliminar">
			</div>

		{{ Form::close() }}

	</div>


	<h3 style="text-align: center;">Editar webhook</h3>

	<div style="width:400px;margin:0 auto;">

		{{ Form::model($webhook, array("route" => array("admin.webhooks.update", $webhook->id) , "method" => "PUT") ) }}

			{{ csrf_field() }}

			<div class="form-group">
				{{ Form::label('event', 'Evento') }}
				{{ Form::text('event', old('event'), array('class' => 'form-control')) }}
			</div>

			<div class="form-group">
				{{ Form::label('url', 'URL') }}
				{{ Form::text('url', old('url'), array('class' => 'form-control')) }}
			</div>

			<div class="form-group" style="text-align: center">
				{{ Form::submit('Guardar', array('class' => 'btn btn-primary')) }}
			</div>


		{{ Form::close() }}

		@include ("layouts.errors")


	</div>

</div>


@endsection