@section('title', 'Crear webhook')

@extends('layouts.app')

@section('content')


<div class="container">
	
    <ol class="breadcrumb panel">
        <li><a href="{{ URL::to('admin') }}">Panel admin</a></li>
        <li><a href="{{ URL::to('admin/webhooks') }}">Webhooks</a></li>
        <li class="active">Crear</li>
    </ol>


	<h3 style="text-align: center;">Crear webhook</h3>
	
	<div style="width:400px;margin:0 auto;">

		{{ Form::open( array("url" => "admin/webhooks", "method" => "post") ) }}

			<div class="form-group">
				{{ Form::label('event', 'Evento') }}
				{{ Form::text('event', old('event'), array('class' => 'form-control', 'required'=>'')) }}
			</div>

			<div class="form-group">
				{{ Form::label('url', 'URL') }}
				{{ Form::text('url', old('url'), array('class' => 'form-control', 'required'=>'')) }}
			</div>

			<div class="form-group" style="text-align: center">
				{{ Form::submit('Enviar', array('class' => 'btn btn-primary')) }}
			</div>


		{{ Form::close() }}

		@include ("layouts.errors")

	</div>


</div>

@endsection