@section('title', 'Webhooks')

@extends('layouts.app')

@section('content')

<div class="container">

    <ol class="breadcrumb panel">
        <li><a href="{{ URL::to('admin') }}">Panel admin</a></li>
        <li class="active">Webhooks</li>
    </ol>


    <div class="form-group">
        
        <a href="{{ URL::to('admin/webhooks/create') }}" class="btn btn-primary">Crear webhook</a>

        {{ Form::open( array("url" => "admin/webhooks/reconstruir", "method" => "post", "style" => "float:right") ) }}
            <input type="button" class="btn btn-success" style="float:right;" value="Reconstruir tabla" onclick="if(confirm('¿Deseas reconstruir la tabla?')) $(this).closest('form').submit()">
        {{ Form::close() }}

    </div>

    @include ("layouts.errors")

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Id tiendanube</th>
                <th>Evento</th>
                <th>URL</th>
                <th>Accion</th>
            </tr>
        </thead>
        <tbody>
            
            @foreach($webhooks as $webhook)

                <tr>
                    <td>{{ $webhook["id"] }}</td>
                    <td>{{ $webhook["id_tnube"] }}</td>
                    <td>{{ $webhook["event"] }}</td>
                    <td>{{ $webhook["url"] }}</td>
                    <td><a href="{{ URL::to('admin/webhooks/' . $webhook->id) }}">Editar</a></td>
                </tr>

            @endforeach

        </tbody>

    </table>

</div>

@endsection
