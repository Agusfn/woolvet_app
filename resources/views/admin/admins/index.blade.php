@section('title', 'Administrar equipo')


@extends('layouts.app')

@section('content')

<div class="container">


    <ol class="breadcrumb panel">
        <li><a href="{{ URL::to('admin') }}">Panel admin</a></li>
        <li class="active">Administrar admins</li>
    </ol>

    <h3 style="text-align: center; margin: 20px 0 20px 0;" >Administradores</h3>

    <table class="table">
        <thead>
            <tr>
                <th>ID</th>
                <th>Nombre</th>
                <th>E-mail</th>
                <th>Fecha registro</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            
            @foreach($admins as $admin)

                <tr>
                    <td>{{ $admin["id"] }}</td>
                    <td>{{ $admin["name"] }}</td>
                    <td>{{ $admin["email"] }}</td>
                    <td>{{ date("d/m/Y", strtotime($admin["created_at"])) }}</td>
                    <td>
                        {{ Form::open( array("url" => "admin/admins/removeadmin/".$admin->id, "method" => "post", "style" => "text-align:center") ) }}
                            {{ csrf_field() }}
                            {{ Form::submit('Remover de admin', array('class' => 'btn btn-warning')) }}
                        {{ Form::close() }}
                    </td>
                </tr>

            @endforeach

        </tbody>

    </table>



</div>

@endsection
