@section('title', 'Hacer admin')

@extends('layouts.app')

@section('content')

    <div class="container">

    	<a href="{{ URL::to('admin/marcas') }}">Volver a marcas</a>

    	<h3 style="text-align:center;">¿Hacer admin a {{ $user->name }} ({{ $user->email }}) ?</h3>

	    {{ Form::open( array("url" => "admin/admins/setadmin/".$user->id, "method" => "post", "style" => "text-align:center") ) }}

	        {{ csrf_field() }}
	        {{ Form::submit('Confirmar', array('class' => 'btn btn-primary')) }}

	    {{ Form::close() }}

    </div>

@endsection
