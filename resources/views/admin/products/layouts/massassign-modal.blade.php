<div class="modal fade" id="assign-modal" tabindex="-1" role="dialog" aria-labelledby="assign-modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="assign-modal-label">Asociar productos con una marca</h4>
            </div>

            <div class="modal-body">
                
                <div class="form-group">
                    <label>Se asociarán los siguientes <span id="product_count"></span> productos:</label>
                    <ul id="assign-modal-productlist"></ul>
                </div>


                {{ Form::open( array("url" => "admin/productos/asignar", "method" => "post", "id" => "assign-form") ) }}

                    <div class="form-group">
                        <label>Marca:</label>

                        <select class="form-control" name="brand_id" id="brand-input">

                            <option value="-1">Seleccionar</option>
                            @foreach($brandList as $brand)

                                <option value="{{ $brand->id }}">{{ $brand->name }}</option>

                            @endforeach

                        </select>
                    </div>

                    <input type="hidden" name="json_product_ids" id="product-ids-input" value="">
                
                {{ Form::close() }}

            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="assign-submit">Aceptar</button>
            </div>
        </div>
    </div>
</div>