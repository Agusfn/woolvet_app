@section('title', 'Productos')


@extends('layouts.app')

@section('content')


@include("layouts.product-list-filters.filter-modal")



<div class="container">

    <ol class="breadcrumb panel">
        <li><a href="{{ URL::to('admin') }}">Panel admin</a></li>
        <li class="active">Productos</li>
    </ol>

    <div class="row" style="margin:20px 0;">

        <div class="col-sm-2 col-sm-offset-4" style="font-size:24px">Productos</div>

        <div class="col-sm-4 col-sm-offset-2" style="text-align: right">
            <button class="btn btn-primary" id="agregar_producto" style="display: inline-block;">Agregar producto</button>
            <a href="{{ config('app.tnube_adminpanel_url').'/products' }}" target="_blank" class="btn btn-success" style="display: inline-block;">Ver en Tiendanube</a>
        </div>

        {{ Form::open( array("url" => "admin/productos/agregar", "method" => "post", "id" => "add_product_form") ) }}
            {{ Form::hidden('id_producto_tnube', '', array('id' => 'id_producto_tnube')) }}
        {{ Form::close() }}

    </div>

    @include('layouts.errors')

    @if(Session::has('message'))
        <div class="alert alert-info">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            {{ Session::get('message') }}
        </div> 
    @endif


    @if ($unassignedCount > 0)
        <div class="alert alert-warning" style="text-align: center; font-size:17px;">
           Hay {{ $unassignedCount }} productos con marcas sin asignar. <a href="{{ URL::to('admin/productos/asignar') }}">Asignar ahora</a>.
        </div>
    @endif
    


    @include("layouts.product-list-filters.filter-sort-buttons")

    <table class="table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Marca</th>
                <th>Categoría</th>
                <th>Variantes</th>
                <th>ID</th>
                <th>ID Tiendanube</th>
            </tr>
        </thead>
        <tbody>
            
            @foreach($products as $product)

                <tr>
                    <td>
                        <a href="{{ URL::to('admin/productos/' . $product->id) }}">
                            {{ $product->nombre }}
                        </a>
                    </td>                    
                    <td>
                        @if ($product->brand != null)
                            {{ $product->brand->name }}
                        @else
                            <strong>No asignada</strong>
                        @endif
                    </td>
                    <td>
                        {{ $product->categoria }}
                    </td>
                    <td>
                        {{ $product->variantes }}
                    </td>
                    <td>{{ $product->id }}</td>
                    <td><a href="{{ config('app.tnube_adminpanel_url').'/products/'.$product->id_producto_tnube }}" target="_blank">{{ $product->id_producto_tnube }}</a></td>
                </tr>

            @endforeach

        </tbody>

    </table>

    <div style="text-align: center;">
        {{ $products->links() }}
    </div>

</div>

@endsection


@section("additional_scripts")

    @include("layouts.product-list-filters.filter-scripts")

    <script type="text/javascript">

        $(document).ready(function() {
            
            $("#agregar_producto").click(function() {

                var id = prompt("Ingrese el ID del producto de tiendanube:");

                if(id != null)
                {
                    $("#id_producto_tnube").val(id);
                    $("#add_product_form").submit();
                }

            });
        });

    </script>

@endsection