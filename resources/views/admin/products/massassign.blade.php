@section('title', 'Asignar marca a productos')


@extends('layouts.app')

@section('content')


@include("admin.products.layouts.massassign-modal")

<div class="container">


    <ol class="breadcrumb panel">
        <li><a href="{{ URL::to('admin') }}">Panel admin</a></li>
        <li><a href="{{ URL::to('admin/productos') }}">Productos</a></li>
        <li class="active">Asignar marca</li>
    </ol>


    @include("layouts.errors")

    <div class="form-group clearfix">
        <div style="float:right"><button class="btn btn-success" id="assign-btn" disabled>Asignar a marca</button></div>
    </div>



    <table class="table">
        <thead>
            <tr>
                <th></th>
                <th>Nombre</th>
                <th>ID</th>
                <th>ID Tiendanube</th>
            </tr>
        </thead>
        <tbody>
            
            @foreach($products as $product)

                <tr>
                    <td>
                        <input type="checkbox" class="product-checkbox" product-id="{{ $product->id }}" product-name="{{ $product->nombre }}">
                    </td>
                    <td class="product-name">{{ $product->nombre }}</td>
                    <td><a href="{{ URL::to('admin/productos/' . $product->id) }}" target="_blank">{{ $product->id }}</a></td>
                    <td><a href="{{ config('app.tnube_adminpanel_url').'/products/'.$product->id_producto_tnube }}" target="_blank">{{ $product->id_producto_tnube }}</a></td>
                </tr>

            @endforeach

        </tbody>

    </table>

</div>

@endsection


@section('additional_style')
    <link href="{{ asset('css/admin/products/assign-brand-pg.css') }}" rel="stylesheet">
@endsection


@section('additional_scripts')
    <script type="text/javascript" src="{{ asset('js/admin/products/assign-brand-pg.js') }}"></script>
@endsection