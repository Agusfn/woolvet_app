@section('title', 'Ventas')

@extends('layouts.app')

@section('content')


@include("layouts.order-list-filters.filter-modal")



<div class="container">

    <ol class="breadcrumb panel">
        <li><a href="{{ URL::to('admin') }}">Panel admin</a></li>
        <li class="active">Ventas</li>
    </ol>


    <h4 style="text-align: center; margin: 20px 0;" class="clearfix">
        Ventas concretadas (individuales)
        <a href="{{ config('app.tnube_adminpanel_url').'/orders' }}" target="_blank" class="btn btn-success" style="float: right; margin-top:-11px">Ver en Tiendanube</a>
    </h4>


    <div class="form-group">
        <button class="btn btn-primary" data-toggle="modal" data-target="#filter-modal"><span class="glyphicon glyphicon-filter"></span>&nbsp;&nbsp;Filtrar</button>
    </div>



    <table class="table">
        <thead>
            <tr>
                <th></th>
                <th>Nro orden</th>
                <th>Fecha</th>
                <th>Producto</th>
                <th>Precio unit.</th>
                <th>Cantidad</th>
                <th>Comprador</th>

            </tr>
        </thead>
        <tbody>
            
            @foreach($sales as $sale)

                <tr>

                    <td><a href="{{ URL::to('admin/ventas/'.$sale->id) }}">Ver más</a></td>
                    <td><a href="{{ config('app.tnube_adminpanel_url').'/orders/'.$sale->id_orden_tnube }}" target="_blank">#{{ $sale->nro_orden_tnube }}</a></td>
                    <td>{{ $sale->fecha_concretada->format('d/m/Y') }}</td>
                    <td>
                        @if($sale->product != null)
                            <a href="{{ URL::to('admin/productos/'.$sale->product->id) }}">{{ $sale->nombre_producto }}</a>
                        @else
                            {{ $sale->nombre_producto }}
                        @endif
                        
                    </td>
                    <td>${{ $sale->precio_venta }}</td>
                    <td>{{ $sale->cantidad }}</td>
                    <td>
                        @if($sale->customer != null)
                            <a href="{{ URL::to('admin/clientes/'.$sale->customer->id) }}">{{ $sale->customer->email }}</a>
                        @endif
                    </td>

                </tr>
               
            @endforeach

        </tbody>

    </table>

    <div style="text-align: center;">
        {{ $sales->links() }}
    </div>

</div>

@endsection



@section('additional_scripts')

    <script type="text/javascript" src="{{ asset('resources/vendor/bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('resources/vendor/bootstrap-datepicker-1.6.4-dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
    @include("layouts.order-list-filters.filter-scripts")

@endsection


@section('additional_style')
    <link href="{{ asset('resources/vendor/bootstrap-datepicker-1.6.4-dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet">
@endsection