@section('title', 'Venta #'.$venta->nro_orden_tnube)

@extends('layouts.app')

@section('content')

<div class="container">

    <ol class="breadcrumb panel">
        <li><a href="{{ URL::to('admin') }}">Panel admin</a></li>
        <li><a href="{{ URL::to('admin/ventas') }}">Ventas</a></li>
        <li class="active">#{{ $venta->nro_orden_tnube }}</li>
    </ol>

    <div class="panel panel-default">
        <div class="panel-body">

            <div class="row">

                <div class="col-sm-4">
                    <label>Pertenece a orden:</label><br/>
                    <span><a href="{{ config('app.tnube_adminpanel_url').'/orders/'.$venta->id_orden_tnube }}" target="_blank">#{{ $venta->nro_orden_tnube }}</a></span>
                    &nbsp;({{ $orderSaleCount }} ventas)
                </div>
                <div class="col-sm-2">
                    <label>Fecha concretada:</label><br/>
                    <span>{{ $venta->fecha_concretada->format('d/m/Y') }}</span>
                </div>

                <div class="col-sm-6">
                    <label>Forma de pago:</label><br/>
                    <span>{{ $venta->forma_pago }}</span>
                </div>
            </div>


            <hr style="margin-top: 33px">

            <div class="row" style="margin-top: 40px">

                <div class="col-sm-4">
                    <label>Producto:</label><br/>
                    <span>
                        @if($venta->product != null)
                            <a href="{{ URL::to('admin/productos/'.$venta->product->id) }}">{{ $venta->nombre_producto }}</a>
                        @else
                            {{ $venta->nombre_producto }}
                        @endif
                    </span>
                </div>

                <div class="col-sm-2">
                    <label>Marca:</label><br/>
                    <span>
                        @if($venta->brand != null)
                            <a href="{{ URL::to('admin/marcas/'.$venta->brand->id) }}">{{ $venta->brand->name }}</a>
                        @else
                            No registrada
                        @endif
                    </span>
                </div>

                <div class="col-sm-2">
                    <label>Precio unitario:</label><br/>
                    <span>${{ number_format($venta->precio_venta,2) }}</span>
                </div>

                <div class="col-sm-2">
                    <label>Cantidad:</label><br/>
                    <span>{{ $venta->cantidad }}</span>
                </div>

                <div class="col-sm-2">
                    <label>Subtotal:</label><br/>
                    <span>${{ number_format(round($venta->precio_venta*$venta->cantidad, 2), 2) }}</span>
                </div>

            </div>


        </div>
    </div>

    <div class="panel panel-default">
        <div class="panel-heading"><h3 class="panel-title">Envío</h3></div>
        <div class="panel-body">

            <div class="row">

                <div class="col-sm-4">
                    <label>Forma de envío:</label><br/>
                    <span>{{ $venta->forma_envio }}</span>
                </div>
                <div class="col-sm-4">
                    <label>Costo envío de orden:</label><br/>
                    <span>${{ number_format($venta->costo_envio,2) }}</span>
                </div>
                <div class="col-sm-4">
                    <label>Costo envío comprador:</label><br/>
                    <span>${{ number_format($venta->costo_envio_comprador,2) }}</span>
                </div>

            </div>

            <div class="row" style="margin-top: 40px">
                
                <div class="col-sm-4">
                    <label>Localidad:</label><br/>
                    <span>{{ $venta->localidad_envio }}</span>
                </div>
                <div class="col-sm-4">
                    <label>Ciudad:</label><br/>
                    <span>{{ $venta->ciudad_envio }}</span>
                </div>
                <div class="col-sm-4">
                    <label>Código postal:</label><br/>
                    <span>{{ $venta->cod_postal_envio }}</span>
                </div>

            </div>

        </div>
    </div>

    @if($venta->customer != null)
        <div class="panel panel-default">
            <div class="panel-heading"><h3 class="panel-title" style="display:inline-block;">Comprador</h3>&nbsp;&nbsp;-&nbsp;&nbsp;
                <a href="{{ URL::to('admin/clientes/'.$venta->customer->id) }}">Ver datos del cliente</a></div>
            <div class="panel-body">

                <div class="row">


                    <div class="col-sm-4">
                        <label>Nombre:</label><br/>
                        <span>{{ $venta->customer->nombre }}</span>
                    </div>
                    <div class="col-sm-4">
                        <label>E-mail:</label><br/>
                        <span>{{ $venta->customer->email }}</span>
                    </div>                    <div class="col-sm-4">
                        <label>Teléfono:</label><br/>
                        <span>{{ $venta->customer->telefono }}</span>
                    </div>

                </div>

                <div class="row" style="margin-top: 40px">

                    <div class="col-sm-4">
                        <label>Dirección:</label><br/>
                        <span>{{ $venta->customer->direccion }}</span>
                    </div>
                    <div class="col-sm-4">
                        <label>Localidad:</label><br/>
                        <span>{{ $venta->customer->localidad }}</span>
                    </div>
                    <div class="col-sm-4">
                        <label>Ciudad:</label><br/>
                        <span>{{ $venta->customer->ciudad }}</span>
                    </div>

                </div>

            </div>
        </div>
    @endif

</div>

@endsection


@section('additional_style')

<style>
    .panel .panel-body
    {
        padding:40px;
    }
    .panel span
    {
        font-size:16px;
    }
</style>

@endsection