@section('title', 'Cuenta registrada')

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body" style="text-align: center;">
                    <span style="font-size:18px;">Tu cuenta ha sido registrada.</span><br/>
                    El siguiente paso es verificar tu dirección e-mail ingresando al mensaje que acabamos de enviar a tu correo <strong>{{ $email }}</strong> y siguiendo las instrucciones.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
