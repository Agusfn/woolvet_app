@section('title', 'Verificación e-mail')

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                <div class="panel-body" style="text-align: center;">
                    @if($success)
                        <div class="alert alert-success"><h4>La dirección e-mail de la cuenta se ha verificado correctamente.</h4></div>
                        <h4>El último paso es esperar nuestra confirmación de tu cuenta, debería ser en menos de 24hs habiles, si no recibís la confirmación, contactanos: <strong>{{ config("mail.brand_support_email") }}</strong></h4>
                    @else
                        <h4>No se pudo verificar la dirección e-mail de la cuenta.</h4>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
