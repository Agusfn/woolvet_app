@section('title', 'Mis productos')


@extends('layouts.app')

@section('content')


@include("layouts.product-list-filters.filter-modal")



<div class="container">

    <ol class="breadcrumb panel">
        <li><a href="{{ URL::to('panel') }}">Panel de administración</a></li>
        <li class="active">Mis productos</li>
    </ol>


    <h3 style="text-align: center; margin: 20px 0 20px 0;">Mis productos</h3>

    @include("layouts.product-list-filters.filter-sort-buttons")

    <table class="table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Categoría</th>
                <th>Variantes</th>
            </tr>
        </thead>
        <tbody>
            
            @foreach($products as $product)

                <tr>
                    <td>
                        <a href="{{ URL::to('panel/productos/' . $product->id) }}">
                            {{ $product->nombre }}
                        </a>
                    </td>                    
                    <td>
                        {{ $product->categoria }}
                    </td>
                    <td>
                        {{ $product->variantes }}
                    </td>
                </tr>

            @endforeach

        </tbody>

    </table>

    <div style="text-align: center;">
        {{ $products->links() }}
    </div>

</div>

@endsection


@section("additional_scripts")

    @include("layouts.product-list-filters.filter-scripts")

@endsection