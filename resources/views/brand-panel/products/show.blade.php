@section('title', $product->nombre)

@extends('layouts.app')

@section('content')

<div class="container">

    <ol class="breadcrumb panel">
        <li><a href="{{ URL::to('panel') }}">Panel de administración</a></li>
        <li><a href="{{ URL::to('panel/productos') }}">Mis productos</a></li>
        <li class="active">{{ $product->nombre }}</li>
    </ol>

    <div class="panel panel-default">
        <div class="panel-body" style="padding: 50px 15px;">


            <div class="row">

                <div class="col-md-3 col-sm-3">
                    <img src="{{ $product->img_ilustrativa }}" alt="imagen producto" style="width:100%;max-width:300px;">
                </div>

                <div class="col-md-9 col-sm-9">

                    <div class="row">

                        <div class="col-md-9 col-sm-9">
                            <h3>{{ $product->nombre }}</h3>

                            <h4 style="font-size: 16px;color: #888;">{{ $product->categoria }}</h4>
                        </div>

                        <div class="col-md-3 col-sm-3">

                            @if ($product->publicado)
                                <a class="btn btn-primary" href="{{ $product->url_tienda }}" target="_blank"><span class="glyphicon glyphicon-new-window"></span>&nbsp;&nbsp;Ver en tienda</a>
                            @else
                                <span class="label label-info" style="font-size:15px;">Oculto</span>
                            @endif

                        </div>

                    </div>


                    <div class="row" style="margin-top:25px">

                        <div class="col-md-12">

                            <h3 style="text-align: center;">Variantes</h3>

                            <table class="table table-stripped">

                                <thead>
                                    <th>Variante</th>
                                    <th>Precio</th>
                                    <th>Precio promocional</th>
                                    <th>Stock</th>
                                </thead>
                                <tbody>
                                    
                                    @foreach($product->variants as $variant)
                                        <tr>
                                            <td>
                                                @if($variant->variante_1 == "" && $variant->variante_2 == "" && $variant->variante_3 == "")
                                                    Unico modelo
                                                @else
                                                    
                                                    @if($variant->variante_1 != "")
                                                        {{ $variant->variante_1 }}
                                                    @endif

                                                    @if($variant->variante_2 != "")
                                                        {{ " / ".$variant->variante_2 }}
                                                    @endif

                                                    @if($variant->variante_3 != "")
                                                        {{ " / ".$variant->variante_3 }}
                                                    @endif      

                                                @endif
                                            </td>
                                            <td>${{ $variant->precio }}</td>
                                            <td>
                                                @if($variant->precio_promocional != 0)
                                                    ${{ $variant->precio_promocional }}
                                                @else
                                                    Sin dto.
                                                @endif
                                            </td>
                                            <td>
                                                @if($variant->stock == -1)
                                                    Sin límite
                                                @elseif($variant->stock == 0)
                                                    Sin stock
                                                @else
                                                    {{ $variant->stock }}
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>


                        </div>


                    </div>


                </div>

            </div>

        </div>
    </div>







</div>

@endsection
