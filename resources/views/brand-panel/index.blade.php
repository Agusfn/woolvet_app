@section('title', 'Panel de administración')

@extends('layouts.app')

@section('content')

    <!--script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: 'Company Performance',
          legend: { position: 'bottom' },
          width: 700,
          height: 500
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script-->


<div class="container">


    <ol class="breadcrumb panel">
        <li class="active">Panel de administración</li>
    </ol>

    <div class="row">

        <div class="col-md-8">
           

            <div class="panel panel-default">
                <div class="panel-heading">Inicio</div>
                
                <div class="panel-body">

                    Estadísticas y más información próximamente

                </div>
            </div>





        </div>

        <div class="col-md-4">

            <div class="panel panel-default">

                <div class="panel-heading">Administrar</div>

                <div class="panel-body">

                    <a href="{{ URL::to('panel/ventas') }}" class="btn btn-primary btn-block">Mis ventas</a>
                    <a href="{{ URL::to('panel/productos') }}" class="btn btn-primary btn-block">Mis productos</a>

                </div>

            </div>

            <div class="panel panel-default">

                <div class="panel-heading">Plataforma</div>

                <div class="panel-body">

                    <a href="{{ URL::to('reportar') }}" class="btn btn-primary btn-block">Informar sugerencias / fallas</a>

                </div>

            </div>

        </div>

    </div>

</div>
@endsection

