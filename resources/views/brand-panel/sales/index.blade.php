@section('title', 'Mis ventas')

@extends('layouts.app')

@section('content')


@include("layouts.order-list-filters.filter-modal")



<div class="container">

    <ol class="breadcrumb panel">
        <li><a href="{{ URL::to('panel') }}">Panel de administración</a></li>
        <li class="active">Mis ventas</li>
    </ol>


    <h3 style="text-align: center; margin: 20px 0;">Mis ventas concretadas</h3>


    <div class="form-group">
        <button class="btn btn-primary" data-toggle="modal" data-target="#filter-modal"><span class="glyphicon glyphicon-filter"></span>&nbsp;&nbsp;Filtrar</button>
    </div>



    <table class="table">
        <thead>
            <tr>
                <th></th>
                <th>Nro orden</th>
                <th>Fecha</th>
                <th>Producto</th>
                <th>Precio unit.</th>
                <th>Cantidad</th>
                <th>Subtotal</th>
            </tr>
        </thead>
        <tbody>
            
            @foreach($sales as $sale)

                <tr>

                    <td><a href="{{ URL::to('panel/ventas/'.$sale->id) }}">Ver más</a></td>
                    <td>#{{ $sale->nro_orden_tnube }}</td>
                    <td>{{ $sale->fecha_concretada->format('d/m/Y') }}</td>
                    <td>
                        @if($sale->product != null)
                            <a href="{{ URL::to('panel/productos/'.$sale->product->id) }}">{{ $sale->nombre_producto }}</a>
                        @else
                            {{ $sale->nombre_producto }}
                        @endif
                        
                    </td>
                    <td>${{ number_format($sale->precio_venta, 2) }}</td>
                    <td>{{ $sale->cantidad }}</td>
                    <td>${{ number_format(round($sale->cantidad*$sale->precio_venta, 2), 2) }}</td>

                </tr>
               
            @endforeach

        </tbody>

    </table>

    <div style="text-align: center;">
        {{ $sales->links() }}
    </div>

</div>

@endsection



@section('additional_scripts')

    <script type="text/javascript" src="{{ asset('resources/vendor/bootstrap-datepicker-1.6.4-dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('resources/vendor/bootstrap-datepicker-1.6.4-dist/locales/bootstrap-datepicker.es.min.js') }}"></script>
    @include("layouts.order-list-filters.filter-scripts")

@endsection


@section('additional_style')
    <link href="{{ asset('resources/vendor/bootstrap-datepicker-1.6.4-dist/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet">
@endsection