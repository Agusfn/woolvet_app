@section('title', 'Venta #'.$venta->nro_orden_tnube)


@extends('layouts.app')

@section('content')

<div class="container">

    <ol class="breadcrumb panel">
        <li><a href="{{ URL::to('panel') }}">Panel de administración</a></li>
        <li><a href="{{ URL::to('panel/ventas') }}">Mis ventas</a></li>
        <li class="active">#{{ $venta->nro_orden_tnube }}</li>
    </ol>


    <div class="panel panel-default">
        <div class="panel-body">


            <div class="row">
                <div class="col-sm-6">
                    <label>Número de orden:</label> <span>#{{ $venta->nro_orden_tnube }}</span>
                </div>
                <div class="col-sm-6">
                    <label>Fecha:</label> <span>{{ $venta->fecha_concretada->format('d/m/Y') }}</span>
                </div>
            </div>


            <div class="row" style="margin-top: 40px">


                <div class="col-sm-6">
                    <label>Forma de envío:</label><br/>
                    <span>{{ $venta->forma_envio }}</span>
                </div>

                <div class="col-sm-2">
                    <label>Ciudad de envío:</label><br/>
                    <span>{{ $venta->ciudad_envio }}</span>
                </div>

                <div class="col-sm-4">
                    <label>Forma de pago:</label><br/>
                    <span>{{ $venta->forma_pago }}</span>
                </div>

            </div>
            
            <hr style="margin-top: 33px">

            <div class="row" style="margin-top: 40px">



                <div class="col-sm-6">
                    <label>Producto:</label><br/>
                    <span>{{ $venta->nombre_producto }}</span>
                </div>

                <div class="col-sm-2">
                    <label>Precio unitario:</label><br/>
                    <span>${{ number_format($venta->precio_venta, 2) }}</span>
                </div>

                <div class="col-sm-2">
                    <label>Cantidad:</label><br/>
                    <span>{{ $venta->cantidad }}</span>
                </div>

                <div class="col-sm-2">
                    <label>Subtotal:</label><br/>
                    <span>${{ number_format(round($venta->precio_venta*$venta->cantidad, 2), 2) }}</span>
                </div>

            </div>


        </div>
    </div>




</div>

@endsection


@section('additional_style')

<style>
    .panel .panel-body
    {
        padding:40px;
    }
    .panel span
    {
        font-size:16px;
    }
</style>

@endsection