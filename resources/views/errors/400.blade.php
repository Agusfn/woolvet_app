@section('title', 'No se pudo procesar la solicitud')

@extends('layouts.app')

@section('content')

	<div class="container" style="text-align: center;">
		<h3>Hubo un problema con la solicitud</h3>
		<a href="{{ URL::to('/') }}">Ir a página principal</a>
	</div>
	
@endsection