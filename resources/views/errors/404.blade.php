@section('title', '404')

@extends('layouts.app')

@section('content')

	<div class="container" style="text-align: center;">
		<h3>No se encontró el recurso solicitado</h3>
		<a href="{{ URL::to('/') }}">Ir a página principal</a>
	</div>
	
@endsection