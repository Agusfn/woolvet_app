<script type="text/javascript">

    $(document).ready(function() {

        // Setear inputs si hay filtros

        @if($admin && request("marca") !== null)
            $("#brand-filter-input").val({{ request("marca") }});
        @endif      

        @if(request("categoria") !== null)
            $("#category-filter-input").val("{{ htmlspecialchars(request('categoria')) }}");
        @endif   

        @if(request("solo_visibles") == 1)
            $("#visible-product-filter").prop("checked", true);
        @endif 

        @if(request("ordenar") !== null)
            $("#sort-products-input").val("{{ htmlspecialchars(request('ordenar')) }}");
        @endif


        $("#apply-filter-btn").click(function() {

            var url = location.protocol + '//' + location.host + location.pathname; // Sin parametros GET

            @if($admin)
                if($("#brand-filter-input")[0].selectedIndex != 0)
                {
                    url = updateURLParameter(url, "marca", $("#brand-filter-input").val());
                }
            @endif

            if($("#category-filter-input")[0].selectedIndex != 0)
            {
                url = updateURLParameter(url, "categoria", $("#category-filter-input").val());
            }

            if($("#visible-product-filter").is(':checked'))
            {
                url = updateURLParameter(url, "solo_visibles", 1);
            }

            window.location.href = url;
        });


        $("#sort-products-input").change(function() {
            window.location.href = updateURLParameter(window.location.href, "ordenar", $(this).val());
        });

    });


    function updateURLParameter(url, param, paramVal){
        var newAdditionalURL = "";
        var tempArray = url.split("?");
        var baseURL = tempArray[0];
        var additionalURL = tempArray[1];
        var temp = "";
        if (additionalURL) {
            tempArray = additionalURL.split("&");
            for (var i=0; i<tempArray.length; i++){
                if(tempArray[i].split('=')[0] != param){
                    newAdditionalURL += temp + tempArray[i];
                    temp = "&";
                }
            }
        }

        var rows_txt = temp + "" + param + "=" + paramVal;
        return baseURL + "?" + newAdditionalURL + rows_txt;
    }

</script>