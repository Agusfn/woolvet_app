<div class="modal fade" id="filter-modal" tabindex="-1" role="dialog" aria-labelledby="filter-modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="filter-modal-label">Filtrar productos</h4>
            </div>

            <div class="modal-body">
                
                <div style="width:400px;margin:0 auto;">

                    @if($admin)
                        <div class="form-group">
                            <label>Marca:</label>
                            <select class="form-control" id="brand-filter-input">
                                <option>Todas</option>
                                @foreach($brandList as $brand)
                                    <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif

                    <div class="form-group">
                        <label>Categoría:</label>
                        <select class="form-control" id="category-filter-input">
                            <option>Todas</option>
                            @foreach($categoryList as $category)
                                <option value="{{ $category }}">{{ $category }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="checkbox">
                        <label><input type="checkbox" id="visible-product-filter"> Sólo productos visibles en la tienda</label>
                    </div>

                </div>

            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="apply-filter-btn">Aplicar</button>
            </div>
        </div>
    </div>
</div>