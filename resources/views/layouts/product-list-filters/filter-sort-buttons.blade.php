<div class="form-group clearfix">

    <div style="float:left">

        <button class="btn btn-primary" data-toggle="modal" data-target="#filter-modal" style="margin-top:20px;"><span class="glyphicon glyphicon-filter"></span>&nbsp;&nbsp;Filtrar</button>

    </div>

    <div style="float:right;">
        Ordenar por:
        <select class="form-control" id="sort-products-input">
            <option value="nombre">Nombre (A-Z)</option>
            @if($admin)
            <option value="marca">Marca (A-Z)</option>
            @endif
            <option value="mayor_variantes">Mayor nro de variantes</option>
            <option value="mas_vendidos">Más vendidos</option>
            <option value="mas_recientes">Más reciente</option>
            <option value="ult_modificados">Ultima vez modif.</option>
        </select>
    </div>
</div>