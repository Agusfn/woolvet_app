<div class="modal fade" id="filter-modal" tabindex="-1" role="dialog" aria-labelledby="filter-modal-label">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="filter-modal-label">Filtrar ventas</h4>
            </div>

            <div class="modal-body">
                
                <div class="row"><div class="col-sm-8 col-sm-offset-2">

                    @if($admin)
                        <div class="form-group">
                            <label>Marca:</label>
                            <select class="form-control" id="brand-filter-input">
                                <option>Todas</option>
                                @foreach($brandList as $brand)
                                    <option value="{{ $brand->id }}">{{ $brand->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif

                    <div class="form-group">

                        <div class="checkbox">
                            <label><input type="checkbox" id="filter-date-range"> Rango de fechas</label>
                        </div>                        

                        <div class="input-group input-daterange" data-provide="datepicker">

                            <input type="text" class="form-control" id="filter-date-from"
                            @if(request("desde") != null)
                                value="{{ request('desde') }}" 
                            @endif
                             disabled>

                            <div class="input-group-addon">&nbsp;a&nbsp;</div>

                            <input type="text" class="form-control" id="filter-date-to" 
                            @if(request("hasta") != null)
                                value="{{ request('hasta') }}" 
                            @endif
                            disabled>

                        </div>
                    </div>

                </div></div>

            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="apply-filter-btn">Aplicar</button>
            </div>
        </div>
    </div>
</div>