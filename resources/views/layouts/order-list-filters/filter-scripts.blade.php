<script type="text/javascript">

    $(document).ready(function() {


        $("#filter-date-from, #filter-date-to").datepicker({
            format:'dd-mm-yyyy',
            language:'es'
        });

        $("#filter-date-range").change(function() {

            if($(this).prop("checked"))
                $("#filter-date-from, #filter-date-to").prop("disabled", false);
            else
                $("#filter-date-from, #filter-date-to").prop("disabled", true);

        });

        $("#apply-filter-btn").click(function() {

            var url = location.protocol + '//' + location.host + location.pathname; // Sin parametros GET

            @if($admin)
                if($("#brand-filter-input")[0].selectedIndex != 0)
                {
                    url = updateURLParameter(url, "marca", $("#brand-filter-input").val());
                }
            @endif

            if($("#filter-date-range").prop("checked"))
            {
                if($("#filter-date-from").val() != "")
                    url = updateURLParameter(url, "desde", $("#filter-date-from").val());

                if($("#filter-date-to").val() != "")
                    url = updateURLParameter(url, "hasta", $("#filter-date-to").val());
            }

            window.location.href = url;

        });


        @if(request("marca") !== null)
            $("#brand-filter-input").val({{ request("marca") }});
        @endif 

        @if(request("desde") !== null || request("hasta") !== null)

            $("#filter-date-range").prop("checked", true).trigger("change");

        @endif

    });


    function updateURLParameter(url, param, paramVal){
        var newAdditionalURL = "";
        var tempArray = url.split("?");
        var baseURL = tempArray[0];
        var additionalURL = tempArray[1];
        var temp = "";
        if (additionalURL) {
            tempArray = additionalURL.split("&");
            for (var i=0; i<tempArray.length; i++){
                if(tempArray[i].split('=')[0] != param){
                    newAdditionalURL += temp + tempArray[i];
                    temp = "&";
                }
            }
        }

        var rows_txt = temp + "" + param + "=" + paramVal;
        return baseURL + "?" + newAdditionalURL + rows_txt;
    }


</script>