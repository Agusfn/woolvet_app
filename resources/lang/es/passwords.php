<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Las contraseñas deben tener al menos seis caracteres y deben coincidir.',
    'reset' => 'Tu contraseña ha sido reestablecida!',
    'sent' => 'Te hemos enviado un e-mail con el link para reestablecer la contraseña!',
    'token' => 'El token suministrado para reestablecer la contraseña es inválido.',
    'user' => "No encontramos ningun usuario con esa dirección e-mail.",

];
