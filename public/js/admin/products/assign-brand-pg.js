
$(document).ready(function() {


	// Boton click en nombre de un producto

	$(".product-name").click(function() {
		$(this).closest("tr").find(".product-checkbox").click();
	});



	$(".product-checkbox").click(function() {

		var count = $('.product-checkbox:checked').length;

		if(count > 0)
			$("#assign-btn").prop("disabled", false);
		else
			$("#assign-btn").prop("disabled", true);

	});


	// Botón abrir modal

	$("#assign-btn").click(function() {

		$("#assign-modal-productlist").empty();

		var product_ids = [];
		
		$(".product-checkbox:checked").each(function () {

			$("#assign-modal-productlist").append("<li>" + $(this).attr("product-name") + "</li>");
			product_ids.push($(this).attr("product-id"));

		});

		$("#product-ids-input").val(JSON.stringify(product_ids));
		$("#product_count").text(product_ids.length);

		$("#assign-modal").modal("show");

	});


	// Boton submit

	$("#assign-submit").click(function() {

		var index = $("#brand-input").prop('selectedIndex');

		if(index == 0)
		{
			alert("Selecciona una marca.");
			return;
		}

		$("#assign-form").submit();

	});



});