<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();


Route::get('/', 'HomeController@index')->name('home');


Route::get('reportar', 'ReportBugController@index')->name("reportar-falla");
Route::post('reportar', 'ReportBugController@submit');



/****** Usuarios/cuenta *******/


Route::get("cuenta", "UserSettingsController@index")->name("usersettings");
Route::get("cuenta/password", "UserSettingsController@changepasswordform")->name("usersettings.changepassword");
Route::post("cuenta/password", "UserSettingsController@changepassword");

Route::get("cuenta/verificar/{token}", "Auth\VerifyEmailController@verify");



/****** Panel marca ********/


Route::get("panel", "BrandPanelController@index")->name("brandpanel.index");

Route::get("panel/productos", "ProductsController@index")->name("brandpanel.products.index");
Route::get("panel/productos/{id}", "ProductsController@show")->name("brandpanel.products.show");

Route::get("panel/ventas", "SalesController@index")->name("brandpanel.sales.index");
Route::get("panel/ventas/{id}", "SalesController@show")->name("brandpanel.sales.show");


/******* ADMIN PANEL *******/

Route::get("admin", "AdminPanelController@index")->name("adminpanel.index");

Route::get("admin/apitest", "AdminPanelController@test");
	

/* Productos */

Route::get("admin/productos", "ProductsController@index")->name("admin.products.index");
Route::get("admin/productos/{id}", "ProductsController@show")->where('id', '[0-9]+')->name("admin.products.show");

Route::get("admin/productos/asignar", "ProductsController@massassign")->name("admin.products.massassign");
Route::post("admin/productos/asignar", "ProductsController@applyassignation");

Route::post("admin/productos/agregar", "ProductsController@addproduct");

/* Ventas */


Route::get("admin/ventas", "SalesController@index")->name("admin.sales.index");
Route::get("admin/ventas/{id}", "SalesController@show")->name("admin.sales.show");


/* Admins */

Route::get("admin/admins", "AdminsController@index")->name("admin.admins.index");

Route::get("admin/admins/setadmin/{id}", "AdminsController@asksetadmin");
Route::post("admin/admins/setadmin/{id}", "AdminsController@setadmin");
Route::post("admin/admins/removeadmin/{id}", "AdminsController@removeadmin");

/* Marcas */

Route::get("admin/marcas", "BrandsController@index")->name("admin.brands.index");
Route::get("admin/marcas/{id}", "BrandsController@show")->name("amdin.brands.show");

Route::post("admin/marcas/confirmar/{id}", "BrandsController@confirm");


/* Clientes */

Route::get("admin/clientes", "CustomersController@index")->name("admin.customers.index");

Route::get("admin/clientes/actualizar", "CustomersController@checknewcustomers");

Route::get("admin/clientes/{id}", "CustomersController@show")->name("admin.customers.show");



/* Webhooks */

Route::get("admin/webhooks", "WebHooksController@index")->name("admin.webhooks.index");
Route::get("admin/webhooks/create", "WebHooksController@create")->name("admin.webhooks.create");
Route::post("admin/webhooks", "WebHooksController@store")->name("admin.webhooks.store");
Route::get("admin/webhooks/{id}", "WebHooksController@edit")->name("admin.webhooks.edit");
Route::put("admin/webhooks/{id}", "WebHooksController@update")->name("admin.webhooks.update");
Route::delete("admin/webhooks/{id}", "WebHooksController@destroy")->name("admin.webhooks.destroy");

Route::post("admin/webhooks/reconstruir", "WebHooksController@reconstruir");