<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});




/****** API PARA USO CON WEBHOOKS DE TIENDANUBE ******/


/* ORDER */

Route::post("tnubewebhook/order/fulfilled", "Api\WebHookEventsController@orderfulfilled");


/* PRODUCT */

Route::post("tnubewebhook/product/created", "Api\WebHookEventsController@productcreated");
Route::post("tnubewebhook/product/updated", "Api\WebHookEventsController@productupdated");
Route::post("tnubewebhook/product/deleted", "Api\WebHookEventsController@productdeleted");


//Route::post("tnubewebhook/product/deleted", "WebHookEventsController@productdeleted");
