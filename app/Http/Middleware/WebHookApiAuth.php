<?php

namespace App\Http\Middleware;

use Closure;
use App\Library\TiendaNubeApi\TiendaNubeApi;
use Illuminate\Support\Facades\Log;
use App;

class WebHookApiAuth
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        Log::info("WebHook Request. Ip: ".$request->ip().". Path: ".$request->path());

        if(App::environment("production"))
        {

            $data = $request->getContent();
            $hmac_header = isset($_SERVER["HTTP_X_LINKEDSTORE_HMAC_SHA256"]) ? $_SERVER["HTTP_X_LINKEDSTORE_HMAC_SHA256"] : "";

            if($hmac_header == hash_hmac('sha256', $data, config("app.app_secret"))) 
                return $next($request);
            else
            {
                Log::info("WebHook AUTH FAILED. Ip: ".$request->ip().". Path: ".$request->path().". Hmac header: ".$hmac_header . "\n Data provided: '" . $data . "'");
                return response(array("success" => false, "message" => "Auth failed"), 401);
            }
        }
        else
        {
            return $next($request);
        }

        
    }
}
