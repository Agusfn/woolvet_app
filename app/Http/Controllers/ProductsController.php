<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use App\Product;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Library\TiendaNubeApi\TiendaNubeApi;
use App\Library\TiendaNubeApi\TNubeApiFunctions;

class ProductsController extends Controller
{

	const RESULTS_PER_PAGE = 20;
    
    public function __construct()
    {
    	$this->middleware("auth", ["only" => ["index", "show"]]);
    	$this->middleware("adminonly", ["only" => ["massassign", "applyassignation"]]);
    }


	public function index()
	{
		
		if(Auth::user()->isAdmin()) 
			$admin = true;
		else 
			$admin = false;


		if($admin || Auth::user()->isConfirmedBrand())
		{

			if($admin)
			{
				$unassignedCount = Product::unassignedBrand()->count(); 
				$brandList = User::select("id", "name")->confirmedBrands()->orderBy("name")->get();
				$categoryList = Product::pluck("categoria")->unique()->sort()->values()->all();


				$products = Product::query()->with("brand");

				if(request()->has("marca"))
				{
					$products = $products->where("user_id", request("marca"));
				}

			}
			else
			{
				$categoryList = Auth::user()->products()->pluck("categoria")->unique()->sort()->values()->all();

				$products = Auth::user()->products();
			}


			// FILTROS
			if(request()->has("categoria"))
			{
				$products = $products->where("categoria", request("categoria"));
			}

			if(request()->has("solo_visibles"))
			{
				$products = $products->where("publicado", true);
			}

			// ORDEN
			if(request()->has("ordenar"))
			{

				if(request("ordenar") == "nombre")
				{
					$products = $products->orderBy("nombre", "ASC");
				}
				else if($admin && request("ordenar") == "marca") 
				{
					$products = $products->join('users', 'user_id', '=', 'users.id')->orderBy("users.name", "ASC");
				}
				else if(request("ordenar") == "mayor_variantes")
				{
					$products = $products->orderBy("variantes", "DESC");
				}
				/*else if(request("ordenar") == "mas_vendidos")
				{
					 
				}*/
				else if(request("ordenar") == "mas_recientes")
				{
					$products = $products->orderBy("created_at", "DESC");
				}
				else if(request("ordenar") == "ult_modificados")
				{
					$products = $products->orderBy("updated_at", "DESC");
				}

			}
			else
			{
				$products = $products->orderBy("nombre", "ASC");
			}


			$products = $products->paginate(self::RESULTS_PER_PAGE)->appends([

				"marca" => request("marca"),
				"categoria" => request("categoria"),
				"solo_visibles" => request("solo_visibles"),
				"ordenar" => request("ordenar")

			]);


			if($admin)
			{
				$data = array("products", "unassignedCount", "brandList", "categoryList", "admin");
				return view("admin.products.index", compact($data));
			}
			else
			{
				$data = array("products", "categoryList", "admin");
				return view("brand-panel.products.index", compact($data));
			}


		}

		else
			return redirect()->route("home");

	}


	public function show($id)
	{

		if(Auth::user()->isAdmin())
		{
			$product = Product::findOrFail($id);
			return view("admin.products.show", compact("product"));
		}
		else if(Auth::user()->isConfirmedBrand())
		{
			$product = Product::where([
				"id" => $id,
				"user_id" => auth()->id() 
			])->first();

			if($product)
				return view("brand-panel.products.show", compact("product"));
			else
				return view("errors.404");

		}
		else
			return redirect()->route("home");

	}


	public function massassign()
	{
		
		$products = Product::unassignedBrand()->sortBy("nombre");
		$brandList = User::select("id","name")->confirmedBrands()->orderBy("name")->get();

		return view("admin.products.massassign", compact("products", "brandList"));
	}



	/*
		Accion para aplicar asignacion de productos a marcas. 
		Asigna producto a marca y agrega producto a la categoría de la marca.
	*/

	public function applyassignation()
	{
		$productIds = json_decode(request("json_product_ids"));
		$brand = User::findOrFail(request("brand_id"));

		$api = new TiendaNubeApi();
		$apiFunc = new TNubeApiFunctions($api);

		foreach ($productIds as $productId) 
		{

			if($product = Product::find($productId))
			{

				if(App::environment("production"))
				{
					if(!$apiFunc->assignCategoryToProduct($product->id_producto_tnube, $brand->brand_category_tnubeid) ||
					!$apiFunc->setProductTags($product->id_producto_tnube, $brand->name))
						return redirect()->back()->withErrors([ "Error asignando categoría y tag de marca a producto de tnube. Api error: ".$api->error.". Comprobar que el producto no tenga la categoria ni el tag de la marca en panel de tnube." ]);
				}

				$product->brand()->associate($brand);
				$product->save();	

			}

		}

		return redirect()->route("admin.products.massassign");

	}



	/*
		Agrega un producto de tnube en caso que no se haya agregado
	*/
	public function addproduct(Request $request)
	{

		$this->validate($request, ["id_producto_tnube" => "required"]);

		$tnubeId = $request->input("id_producto_tnube");

		$api = new TiendaNubeApi();
		$apiFunc = new TNubeApiFunctions($api);


        if(Product::WithTnubeId($tnubeId)->count() == 0)
        {
            
            if($productData = $api->GetProduct($tnubeId)) 
            {
          		$apiFunc->createProductFromApiData($productData);
          		return redirect()->back()->with("message", "El producto fue agregado correctamente.");
            }
            else
            {
            	return redirect()->back()->withErrors(["Error. ".$api->error]);
            }
            
        }
        else
        	return redirect()->back()->with("message", "El producto ya existe.");

	}




}
