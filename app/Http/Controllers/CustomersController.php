<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;
use App\Library\TiendaNubeApi\TiendaNubeApi;
use App\Library\TiendaNubeApi\TNubeApiFunctions;

class CustomersController extends Controller
{

    public function __construct()
    {
        $this->middleware("adminonly");
    }

    public function index()
    {
    	$customers = Customer::all();

    	return View("admin.customers.index", compact("customers"));
    }


    public function show($id)
    {
    	$customer = Customer::findOrFail($id);
    	
    	return View("admin.customers.show", compact("customer"));
    }


    public function checknewcustomers()
    {

        $api = new TiendaNubeApi();
        $apiFunc = new TNubeApiFunctions($api);

        $newCustomers = $apiFunc->checkNewCustomers();

        if($newCustomers !== false)
            $message = "Se registraron ".$newCustomers." clientes nuevos.";
        else
            $message = "Error registrando nuevos clientes. Msg: ".$apiFunc->error;

        return redirect()->route("admin.customers.index")->with("message", $message);
    }
}
