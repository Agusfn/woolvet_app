<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;

class UserSettingsController extends Controller
{
    public function __construct()
    {
    	$this->middleware("auth");
    }

    public function index()
    {
    	return view("user.settings")->with("user", Auth::user());
    }

    public function changepasswordform()
    {
    	return view("user.changepassword");
    }

    public function changepassword(Request $request)
    {
		$this->validate($request, [
            "old_password" => "required",
            "password" => config('auth.passwords.validation')
        ]);

        $user = Auth::user();

		if (!Hash::check(Input::get('old_password'), $user->password)) { 
			return redirect()->back()->withErrors([ "old_password" => "La contraseña ingresada no es correcta." ]);
		}

        $user->fill([
            'password' => Hash::make($request->password)
        ])->save();


        $request->session()->flash('success', 'Tu contraseña fue cambiada correctamente.');

        return back();
    }

}
