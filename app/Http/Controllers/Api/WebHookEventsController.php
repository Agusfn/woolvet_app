<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Log;
use App\Library\TiendaNubeApi\TiendaNubeApi;
use App\Library\TiendaNubeApi\TNubeApiFunctions;
use App\Sale;
use App\Product;
use App\ProductVariant;
use App\Customer;

class WebHookEventsController extends Controller
{
    
    private $api;
    private $apiFunc;

    private $entityTnubeId; // ID de tiendanube de la cosa que desencadenó el evento
    private $event;

    private $responseError = false;


	public function __construct(Request $request)
	{
		$this->middleware("webhookapiauth"); // Verificar que la key del header es correcta y el origen está autorizado

        $this->api = new TiendaNubeApi();
        $this->apiFunc = new TNubeApiFunctions($this->api);

        $this->entityTnubeId = $request->json("id");
        $this->event = $request->json("event");
	}


    private function response_success()
    {
        return response(["success" => true], 200);
    }

    private function response_error($message = "")
    {
        Log::warning("Error API Tiendanube en evento Webhook ".$this->event.". Msg: ".(!$message ? $this->api->error : $message));

        $result = array("success" => false, "Error obteniendo datos via API tiendanube.");
        
        return response($result, 503);
    }




    /********** ORDERS **********/

	public function orderfulfilled()
	{

        $this->apiFunc->checkNewCustomers();

        if($this->apiFunc->registerTNubeOrder($this->entityTnubeId))
            return $this->response_success();
        else
            return $this->response_error($this->apiFunc->error);

	}


    /********** PRODUCTS **********/


    public function productcreated()
    {

        if(Product::WithTnubeId($this->entityTnubeId)->count() == 0)
        {
            
            if(!$productData = $this->api->GetProduct($this->entityTnubeId)) 
                return $this->response_error();             
 
            $this->apiFunc->createProductFromApiData($productData);
        }

        return $this->response_success();
    }



	public function productupdated()
	{

        if(!$productData = $this->api->GetProduct($this->entityTnubeId)) 
            return $this->response_error();             
        
        $product = Product::WithTnubeId($this->entityTnubeId)->first();

        if($product != null)
            $this->apiFunc->updateProduct($product, $productData);

        return $this->response_success();

	}


    public function productdeleted()
    {
        
        if($product = Product::WithTnubeId($this->entityTnubeId)->first())
        {
            $product->variants()->delete();
            $product->delete();
        }
        return $this->response_success();
    }






}
