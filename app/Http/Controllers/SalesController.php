<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use App\Library\TiendaNubeApi\TiendaNubeApi;
use App\Sale;
use App\User;
use Illuminate\Support\Facades\Auth;

class SalesController extends Controller
{
    
    const RESULTS_PER_PAGE = 20;


    public function __construct()
    {
        $this->middleware("auth", ["only" => ["index", "show"]]);
    }


    public function index()
    {

        if(Auth::user()->isAdmin()) 
            $admin = true;
        else 
            $admin = false;


        if($admin || Auth::user()->isConfirmedBrand())
        {
            
            if($admin)
            {
                $brandList = User::select("id", "name")->confirmedBrands()->orderBy("name")->get();
                $sales = Sale::query();

                if(request()->has("marca"))
                {
                    $sales = $sales->where("user_id", request("marca"));
                }
            }
            else
                $sales = Auth::user()->sales();


            if(request()->has("desde"))
            {
                $sales = $sales->whereDate("fecha_concretada", ">=", date("Y-m-d", strtotime(request("desde"))));
            }

            if(request()->has("hasta"))
            {
                $sales = $sales->whereDate("fecha_concretada", "<=", date("Y-m-d", strtotime(request("hasta"))));
            }


            $sales = $sales->orderBy("fecha_concretada", "DESC")->paginate(self::RESULTS_PER_PAGE)->appends([
                "marca" => request("marca"),
                "desde" => request("desde"),
                "hasta" => request("hasta"),
            ]);


            if($admin)
                return View("admin.sales.index", compact("sales", "brandList", "admin"));
            else
                return View("brand-panel.sales.index", compact("sales", "admin"));

            
        }
        else
            return redirect()->route("home");    	


    }


    public function show($id)
    {
    	if(Auth::user()->isAdmin())
    	{
	    	$venta = Sale::findOrFail($id);

	    	$orderSaleCount = Sale::where("id_orden_tnube", $venta->id_orden_tnube)->count(); // Cantidad de ventas bajo la misma orden

	    	return View("admin.sales.show", compact("venta", "orderSaleCount"));
    	}
    	else if(Auth::user()->isConfirmedBrand())
    	{
            $venta = Sale::where([
                "id" => $id,
                "user_id" => auth()->id() 
            ])->first();	    	

            if($venta)
                return View("brand-panel.sales.show", compact("venta"));
            else 
                return view("errors.404");

    	}
    	else
			return redirect()->route("home");


    }


}
