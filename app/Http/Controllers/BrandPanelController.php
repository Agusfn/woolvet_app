<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BrandPanelController extends Controller
{
    
	public function __construct()
	{
		$this->middleware("confirmedbrandonly");
	}

	// Action 
	public function index()
	{
		return view("brand-panel.index");
	}

}
