<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AdminsController extends Controller
{
    
    public function __construct()
    {
    	$this->middleware("superadminonly");
    }

	public function index()
	{
		$admins = User::Admins();
		return view("admin.admins.index", compact("admins"));
	}

	public function asksetadmin($id)
	{
		$user = User::findOrFail($id);

		if($user->isUnconfirmedBrand())
			return view("admin.admins.setadmin", compact("user"));
		else
			return view("errors.400");

	}


	public function setadmin($id)
	{
		$user = User::findOrFail($id);

		if($user->isUnconfirmedBrand())
		{

			$user->role = "admin";
			$user->confirm_status = "";
			$user->save();

			return redirect()->route("admin.admins.index");

		}
		else
			return view("errors.400");

	}


	public function removeadmin($id)
	{
		$user = User::findOrFail($id);

		if($user->role == "admin")
		{

			$user->role = "brand";
			$user->confirm_status = "unconfirmed";
			$user->save();

			return redirect()->route("admin.admins.index");

		}
		else
			return view("errors.400");

	}



}
