<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\BugReport;
use Illuminate\Support\Facades\Auth;

class ReportBugController extends Controller
{

	public function __construct()
	{
		$this->middleware("confirmedbrandonly");
	}

    public function index()
    {
    	return view("report-bug");
    }


    public function submit(Request $request)
    {
        $this->validate(request(), [
		    "message_type" => "required|in:bug,sugerencia",
		    "mensaje" => "required"
		]);

        \Mail::to("agustin.fernandez@woolvet.com", "Agustin Fernandez Nuñez")->send(
        	new BugReport( Auth::user(), $request->input("message_type"), $request->input("mensaje") )
        );

        return redirect()->back()->with('success', true);
    }


}
