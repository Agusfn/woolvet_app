<?php

namespace App\Http\Controllers;

use App;
use Illuminate\Http\Request;
use App\User;
use App\Library\TiendaNubeApi\TiendaNubeApi;

class BrandsController extends Controller
{

    const RESULTS_PER_PAGE = 10;

    public function __construct()
    {
        $this->middleware("adminonly");
    }

    public function index()
    {

        $unconfirmedBrands = User::UnconfirmedBrands();
        $brands = User::ConfirmedBrands();

        if(request()->has("ordenar"))
        {
            if(request("ordenar") == "recientes")
            {
                $brands = $brands->orderBy("created_at", "DESC");
            }
            else if(request("ordenar") == "antiguas") 
            {
                $brands = $brands->orderBy("created_at", "ASC");
            }
            else if(request("ordenar") == "nombre_asc")
            {
                $brands = $brands->orderBy("name", "ASC");
            }
            else if(request("ordenar") == "nombre_desc")
            {
                 $brands = $brands->orderBy("name", "DESC");
            }
        }
        else
        {
            $brands = $brands->orderBy("created_at", "DESC");
        }


        $brands = $brands->paginate(self::RESULTS_PER_PAGE)->appends("ordenar", request("ordenar")); 

        return view("admin.brands.index", compact("unconfirmedBrands", "brands"));

    }


    public function show($id)
    {
        $user = User::findOrFail($id);

        if($user->role == "brand")
            return view("admin.brands.show", compact("user"));
        else
            return view("errors.404");
    }


    public function destroy($id)
    {
        //
    }

    /*
        Confirmar o rechazar una marca nueva.
        Si se confirma, además se crea una categoría
    */
    public function confirm($id)
    {
        $user = User::findOrFail($id);

        if($user->isUnconfirmedBrand())
        {

            if(request("confirm_status") == "confirmed")
            {

                $api = new TiendaNubeApi();

                if(App::environment("production"))
                {
                    if($category = $api->CreateCategory($api::BRANDS_CAT_ID, $user->name))
                        $user->confirmBrandAccount($category["id"]);
                    else
                    {
                        return redirect()->back()->withErrors([ $api->error ]);
                    }

                }
                else
                    $user->confirmBrandAccount(-1);


            }
            else if(request("confirm_status") == "rejected")
            {
                $user->rejectBrandAccount();
            }
            else
            {
                return view("errors.400");
            }

            return redirect()->route("admin.brands.index");

        }
        else
            return view("errors.400");

    }


}
