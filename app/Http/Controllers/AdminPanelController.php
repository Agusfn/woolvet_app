<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Library\TiendaNubeApi\TiendaNubeApi;

class AdminPanelController extends Controller
{

	public function __construct()
	{
		$this->middleware("adminonly");
	}

    public function index()
    {
    	return view("admin.index");
    }


    public function test()
    {
    	
        \Mail::to(Auth::user())->send(new \App\Mail\AccountRegistered);

        //$api = new TiendaNubeApi();

        //return $api->GetOrders();

    }




}
