<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{


    public function index()
    {

        
        if(Auth::check())
        {
            $user = Auth::user();

            if($user->isAdmin())
                return redirect("admin");

            else if($user->isConfirmedBrand())
                return redirect("panel");

            else 
                return view("user.pending-confirmation");

        }
        else
            return redirect("login");



    }

}
