<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;

class VerifyEmailController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function verify($token)
    {
        
        if($user = User::where("verified_email_token", $token)->first())
        {
            $user->verifyEmail();
            return View("auth.verify-email", ["success" => true]);
        }
        return View("auth.verify-email", ["success" => false]);

    }

}
