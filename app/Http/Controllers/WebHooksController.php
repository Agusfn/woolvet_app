<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\WebHook;
use App\Library\TiendaNubeApi\TiendaNubeApi;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class WebHooksController extends Controller
{

    private $api;

    public function __construct()
    {
        
        $this->middleware("superadminonly");

        $this->api = new TiendaNubeApi();
    }


    public function index()
    {
        $webhooks = WebHook::all();
        return view("admin.webhooks.index", compact("webhooks"));
    }



    public function create()
    {
        return view("admin.webhooks.create");
    }



    public function store()
    {
        $this->validate(request(), [
            "event" => "required",
            "url" => "required"
        ]);
        
        if($request = $this->api->CreateWebHook(request("event"), request("url"))) 
        {

            WebHook::create([
                "id_tnube" => $request["id"],
                "event" => request("event"),
                "url" => request("url")
            ]);

            return redirect()->route("admin.webhooks.index");
        } 
        else 
            return redirect()->back()->withErrors([ "Error API: ".$this->api->error ])->withInput();

    }


    public function edit($id)
    {
        $webhook = WebHook::findOrFail($id);
        return view("admin.webhooks.edit")->with("webhook", $webhook);
    }


    public function update($id)
    {

        $this->validate(request(), [
            "event" => "required",
            "url" => "required"
        ]);

        $webhook = WebHook::findOrFail($id);

        if($this->api->EditWebHook($webhook["id_tnube"], request("event"), request("url"))) 
        {
            $webhook->event = request("event");
            $webhook->url = request("url");
            $webhook->save();

            return redirect()->route("admin.webhooks.index");
        }
        else
            return redirect()->back()->withErrors([ $this->api->error ])->withInput();

    }


    public function destroy($id)
    {
        
        $webhook = WebHook::findOrFail($id);

        if($this->api->DeleteWebHook($webhook["id_tnube"]))
        {
            $webhook->delete();
            return redirect()->route("admin.webhooks.index");
        }
        else
        {  
            return redirect()->back()->withErrors([ $this->api->error ])->withInput([
                "event" => $webhook["event"], 
                "url" => $webhook["url"]
            ]); 
        }

    }


    public function reconstruir()
    {
        $webhooks = $this->api->GetWebHooks();

        if(!$webhooks)
            return redirect()->back()->withErrors([ $this->api->error ]);

        DB::table("webhooks")->truncate();

        $this->populateWebHooks($webhooks);

        return redirect()->route("admin.webhooks.index");
    }


    
    // Crear multiples webhooks y guardarlos, a partir de un array de webhooks desde la API.

    private function populateWebHooks($webhooks)
    {
        foreach($webhooks as $webhook) 
        {
            WebHook::create([
                "id_tnube" => $webhook["id"],
                "event" => $webhook["event"],
                "url" => $webhook["url"]
            ]);
        }
    }

}
