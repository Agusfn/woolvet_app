<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Mail\BrandConfirmed;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'nombre_responsable' ,'email', 'password', 'role', 'confirm_status', 'verified_email', 'verified_email_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    public function products()
    {
        return $this->hasMany(Product::class);
    }

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }





    public static function ScopeUnconfirmedBrands($query)
    {
        return $query->where([
            ["role", "=", "brand"],
            ["confirm_status", "=", "unconfirmed"]
        ])->get();
    }

    public static function ScopeConfirmedBrands($query)
    {
        return $query->where([
            ["role", "=", "brand"],
            ["confirm_status", "=", "confirmed"]
        ]);
    }

    public static function ScopeAdmins($query)
    {
        return $query->where([
            ["role", "=", "admin"]
        ])->get();
    }






    public function isAdmin()
    {
        if($this->role == "superadmin" || $this->role == "admin") 
            return true;
        else 
            return false;
    }

    public function isSuperAdmin()
    {
        if($this->role == "superadmin") 
            return true;
        else 
            return false;
    }


    public function isConfirmedBrand()
    {
        if($this->role == "brand" && $this->confirm_status == "confirmed") 
            return true;
        else 
            return false;
    }

    public function isUnconfirmedBrand()
    {
        if($this->role == "brand" && $this->confirm_status == "unconfirmed") 
            return true;
        else 
            return false;
    }






    public function confirmBrandAccount($brandCategoryId, $sendEmail = true)
    {
        $this->confirm_status = "confirmed";
        $this->brand_category_tnubeid = $brandCategoryId;
        $this->save();

        //if($sendEmail)
          //  \Mail::to($this)->send(new BrandConfirmed($this));

    }

    public function rejectBrandAccount()
    {
        $this->confirm_status = "rejected";
        // enviar e-mail?
        $this->save();
    }

    public function verifyEmail()
    {
        $this->verified_email = true;
        $this->verified_email_token = "";
        $this->save();
    }



}
