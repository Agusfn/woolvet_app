<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Library\TiendaNubeApi\TiendaNubeApi;
use App\Library\TiendaNubeApi\TNubeApiFunctions;

class SaveNewCustomers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'customer:check';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Verificar nuevas cuentas de cliente de tiendanube y registrarlos.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return exit code: 0: sin usuarios nuevos, 1: registrados nuevos usuarios, -1: error
     */
    public function handle()
    {

        $api = new TiendaNubeApi();
        $apiFunc = new TNubeApiFunctions($api);

        $newCustomers = $apiFunc->checkNewCustomers();

        if($newCustomers !== false)
            $this->line("Se registraron ".$newCustomers." clientes nuevos.");
        else
            $this->line("Error registrando nuevos clientes.");

    }
}
