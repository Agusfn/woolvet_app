<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\User;

class BugReport extends Mailable
{
    use Queueable, SerializesModels;


    public $user;
    public $messageType;
    public $message;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $messageType, $message)
    {

        $this->user = $user;
        $this->messageType = $messageType;
        $this->message = $message;        

        $this->replyTo($user->email, $user->nombre_responsable);

        if($messageType == "bug")
        {
            $this->subject = "App Woolvet: Reporte de bug. Enviado por: ".$user->name;
        }
        else // $messageType = "sugerencia"
        {
            $this->subject = "App Woolvet: Sugerencia. Enviada por: ".$user->name;
        }


    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->markdown('emails.bug-report');
    }
}
