<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{


    protected $guarded = [ ];

    protected $dates = ["fecha_concretada"];


    public function customer()
    {
    	return $this->belongsTo(Customer::class, "customer_id");
    }

    public function product()
    {
    	return $this->belongsTo(Product::class, "product_id");
    }

    public function brand()
    {
    	return $this->belongsTo(User::class, "user_id");
    }

    public static function alreadyExists($tNubeOrderId, $tNubeVariantId)
    {
		$saleCount = Sale::where([
			["id_orden_tnube", $tNubeOrderId],
			["id_variante_tnube", $tNubeVariantId]
		])->count();

		if($saleCount == 0)
			return false;
		else
			return true;

    }

    

}
