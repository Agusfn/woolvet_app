<?php

/*
	Clase para API de tiendanube

*/
	
namespace App\Library\TiendaNubeApi;


use Illuminate\Support\Facades\Log;


define("API_URL", "https://api.tiendanube.com/v1/");
define("STORE_ID", "524455");
define("ACCESS_TOKEN", "dd368c8b8b1aa27e8ed0ffbe8a18d72b3516d306");

define("USER_AGENT", "Woolvet App (agusfn20@gmail.com)");


class TiendaNubeApi
{
	private $http_header;
	private $http_post_header;

	public $http_response_no;
	public $error;

	const BRANDS_CAT_ID = 2272601; // ID de la categoría "marcas", para crear subcategorías dentro.


	public function __construct() {

		$this->http_header = array(
			"Authentication: bearer ".ACCESS_TOKEN,
			"User-Agent: ".USER_AGENT
		);

		$this->http_post_header = $this->http_header;
		array_push($this->http_post_header, "Content-Type: application/json; charset=utf-8");
		
	}



	/*
		Realizar solicitud.
		Devuelve un array (parseado del json) si la solicitud es exitosa, sino FALSE, con su respectivo mensaje de error.
	*/
	protected function request($api_resource, $urlparams = "", $method = "GET", $post_data = array())
	{
		
		$ch = curl_init();
		$request_url = API_URL.STORE_ID."/".$api_resource."?".$urlparams;

		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($ch, CURLOPT_CAINFO,  dirname(__FILE__).DIRECTORY_SEPARATOR.'cacert.pem'); 
		curl_setopt($ch, CURLOPT_URL, $request_url);

		if($method == "POST" || $method == "PUT") 
		{
			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post_data));
			curl_setopt($ch, CURLOPT_HTTPHEADER, $this->http_post_header);
		}
		else
			curl_setopt($ch, CURLOPT_HTTPHEADER, $this->http_header);

		

		if(!$result = curl_exec($ch)) 
		{
			$this->error = "Error CURL: " . curl_error($ch);
			curl_close($ch);
			Log::warning("TiendaNubeApi CURL error. Request URL: ".$request_url.". Method: ".$method.". Error: ".$this->error);

			return false;
		}
		else
		{
			$this->http_response_no = curl_getinfo($ch, CURLINFO_HTTP_CODE);
			
			
			if($response = $this->processRequest($result))
				return $response;
			else
			{
				Log::warning("TiendaNubeApi response error. Request URL: ".$request_url.". Method: ".$method.". Resp. code: ".$this->http_response_no.". Post data: ".json_encode($post_data)."\n Error: ".$this->error);
				return false;
			}
		}

	}


	/* 
		Verifica la respuesta de tiendanube de la solicitud. 
		Devuelve si fue exitosa, los datos en Json o TRUE, sino FALSE y el error.
	*/
	private function processRequest($result)
	{
		$code = $this->http_response_no;

		if($code == 200 || $code == 201) // OK / created
		{
			$response = json_decode($result, true);
			if($response != null)
				return $response;
			else return true;
		}
		else
		{

			if($code == 400)
				$this->error = "Solicitud no enviada correctamente. Datos incompletos o identificacion insuficiente.";

			else if($code == 401)
				$this->error = "No autorizado";

			else if($code == 404)
				$this->error = "No se encontró el recurso solicitado";

			else if($code == 415)
				$this->error = "Cabecera HTTP de datos tipo JSON faltante para solicitudes POST y PUT";

			else if($code == 422)
				$this->error = "Alguno de los datos enviados son inválidos";

			else if($code == 500 || $code == 502 || $code == 503 || $code == 504) // IMPLEMENTAR REINTENTO??
				$this->error = "El servidor de tiendanube no responde";

			else if($code == 429)
				$this->error = "Demasiadas solicitudes al servidor de tiendanube";

			else
				$this->error = "Respuesta de HTTP con codigo no identificado";

			return false;
		}



	}


	/*************** WEBHOOKS ***************/



	public function GetWebHooks($urlparams = "") 
	{
		return $this->request("webhooks", $urlparams);
	}

	public function GetWebHook($id)
	{
		return $this->request("webhooks/".$id);
	}


	/* $event: Evento, ver eventos en doc. API tiendanube.
	*/
	public function CreateWebHook($event, $url) 
	{
		$data = array(
			"url" => $url,
			"event" => $event
		);

		return $this->request("webhooks", "", "POST", $data);
	}


	/* $data: array asociativo con data de webhook (ver doc. como se compone)
	*/
	public function EditWebHook($id, $event, $url) 
	{
		$data = array(
			"event" => $event,
			"url" => $url
		);

		return $this->request("webhooks/".$id, "", "PUT", $data);
	}


	public function DeleteWebHook($id)
	{
		return $this->request("webhooks/".$id, "", "DELETE");
	}



	/*************** PRODUCTS ***************/



	public function GetProducts($urlParams = "") 
	{
		return $this->request("products", $urlParams);
	}

	public function GetProduct($id, $urlParams = "") 
	{
		return $this->request("products/".$id, $urlParams);
	}


	public function EditProduct($id, $data)
	{
		return $this->request("products/".$id, "", "PUT", $data);
	}



	/*************** PRODUCT VARIANTS ***************/



	public function GetVariants($productId, $urlParams = "") 
	{
		return $this->request("products/".$productId."/variants", $urlParams);
	}

	public function GetVariant($productId, $variantId, $urlParams = "") 
	{
		return $this->request("products/".$productId."/variants/".$variantId, $urlParams);
	}

	/* Editar una variante.
	   $data: ver documentación API tiendanube
	*/
	public function EditVariant($productId, $variantId, $data)
	{
		return $this->request("products/".$productId."/variants/".$variantId, "", "PUT", $data);
	}

	// La creación y eliminación de variantes por el momento no la vamos a implementar




	/*************** ORDERS ***************/



	public function GetOrders($urlparams = "")
	{
		return $this->request("orders", $urlparams);
	}


	public function GetOrder($id) 
	{
		return $this->request("orders/".$id);
	}



	/*************** CATEGORIES ***************/


	public function GetCategories($urlParams = "")
	{
		return $this->request("categories", $urlParams);
	}


	public function GetCategory($id, $urlParams = "")
	{
		return $this->request("categories/".$id, $urlParams);
	}

	/*
		Crea una categoría/subcategoría. Para que sea categoría ppal, $parentId debe ser cero.
	*/
	public function CreateCategory($parentId, $name_es)
	{

		$data = array(
			"name" => [
				"es" => $name_es
			],
			"parent" => $parentId
		);

		return $this->request("categories", "", "POST", $data);

	}


	//*************** CUSTOMERS ***************/


	public function GetCustomers($urlParams = "")
	{
		return $this->request("customers", $urlParams);
	}


	public function GetCustomer($id, $urlParams = "")
	{
		return $this->request("customers/".$id, $urlParams);
	}



}

