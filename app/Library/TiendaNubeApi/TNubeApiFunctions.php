<?php
/*
	Clase que utiliza la API de tiendanube para almacenar o manipular los modelos en la App Woolvet.

*/
namespace App\Library\TiendaNubeApi;


use App\Sale;
use App\Product;
Use App\ProductVariant;
Use App\Customer;


class TNubeApiFunctions
{

	private $api;
    public $error;


	public function __construct(TiendaNubeApi $api)
	{
		$this->api = $api;
	}



    /*  Registrar ventas de una orden de tiendanube a la DB, a partir de la ID de una orden de tnube concretada.
        Una orden se descompone en una o más ventas que se registran en la app

        $tnubeOrderId:     id de la orden de tiendanube
    */

    public function registerTNubeOrder($tnubeOrderId)
    {


        if(!$orderData = $this->api->GetOrder($tnubeOrderId))
        {
            $this->error = "Api error: ".$this->api->error;
            return false;
        }

        $registeredSalesCount = Sale::where("id_orden_tnube", $tnubeOrderId)->count(); // Cantidad de ventas registradas de ese pedido. Debería ser cero.

        if($registeredSalesCount < sizeof($orderData["products"]))
        {

            foreach($orderData["products"] as $productSold)
            {

                if(!Sale::alreadyExists($orderData["id"], $productSold["variant_id"]))
                {
                    
                    // Obtenemos datos desde la variante ya que el id de producto de tnube a veces es uno erroneo

                    $variant = ProductVariant::WithTnubeId($productSold["variant_id"])->first();

                    $customerId = Customer::GetIdFromTnubeId($orderData["customer"]["id"]);

                    Sale::create([

                        "product_id" => ($variant->product ? $variant->product->id : -1),
                        "variant_id" => ($variant ? $variant->id : -1),
                        "user_id" => ($variant->product->brand ? $variant->product->brand->id : -1),
                        "customer_id" => ($customerId != null ? $customerId : -1),
                        "id_orden_tnube" => $orderData["id"],
                        "nro_orden_tnube" => $orderData["number"],
                        "id_cliente_tnube" => $orderData["customer"]["id"],
                        "id_producto_tnube" => $productSold["id"],
                        "id_variante_tnube" => $productSold["variant_id"],
                        "fecha_concretada" => date("Y-m-d H:i:s", strtotime($orderData["closed_at"])),
                        "nombre_producto" => $productSold["name"],
                        "precio_venta" => $productSold["price"],
                        "cantidad" => $productSold["quantity"],
                        "forma_pago" => $orderData["payment_details"]["method"],
                        "forma_envio" => $orderData["shipping_option"],
                        "costo_envio_comprador" => $orderData["shipping_cost_customer"],
                        "costo_envio" => $orderData["shipping_cost_owner"],
                        "localidad_envio" => $orderData["shipping_address"]["locality"],
                        "ciudad_envio" => $orderData["shipping_address"]["city"],
                        "cod_postal_envio" => $orderData["shipping_address"]["zipcode"]

                    ]);

                }

            }

        }

        return true;

    }





    /* Crear producto y guardarlo junto con todas sus variantes a partir de la data de API tiendanube

       $productData:     array devuelto de tnubeapi->GetProduct(productid)

    */ 

    public function createProductFromApiData($productData)
    {

        $product = Product::create([
            "id_producto_tnube" => $productData["id"],
            "user_id" => -1,
            "nombre" => $productData["name"]["es"],
            "url_tienda" => $productData["canonical_url"],
            "img_ilustrativa" => !empty($productData["images"]) ? $productData["images"][0]["src"] : "",
            "variantes" => sizeof($productData["variants"]),
            "categoria" => $this->parseCategories($productData["categories"]),
            "publicado" => $productData["published"],
            "eliminado" => false
        ]);

        $this->createVariantsForProduct($product, $productData["variants"]);


    }


    /*
        Actualiza un producto de la base de datos con datos de la API de tiendanube. Elimina las categorías y las vuelve a hacer.
        Actualiza datos propios del producto, no de relaciones del mismo.
    */
    public function updateProduct(Product $product, $productData)
    {

        $product->update([

            "nombre" => $productData["name"]["es"],
            "url_tienda" => $productData["canonical_url"],
            "img_ilustrativa" => $productData["images"][0]["src"],
            "variantes" => sizeof($productData["variants"]),
            "categoria" => $this->parseCategories($productData["categories"]),
            "publicado" => $productData["published"]

        ]);

        $product->variants()->delete();

        $this->createVariantsForProduct($product, $productData["variants"]);
    }



    /*
        Método para crear las variantes de un determinado producto existente en la DB, dado el array de variantes de la API de tnube.
    */
    public function createVariantsForProduct(Product $product, $variants)
    {
        foreach($variants as $variant) 
        {     
            $product->variants()->create([
                "id_variante_tnube" => $variant["id"],
                "id_producto_tnube" => $product->id_producto_tnube,
                "precio" => $variant["price"] ? $variant["price"] : -1,
                "precio_promocional" => !is_null($variant["promotional_price"]) ? $variant["promotional_price"] : 0,
                "stock" => !is_null($variant["stock"]) ? $variant["stock"] : -1,
                "variante_1" => isset($variant["values"][0]) ? $variant["values"][0]["es"] : "",
                "variante_2" => isset($variant["values"][1]) ? $variant["values"][1]["es"] : "",
                "variante_3" => isset($variant["values"][2]) ? $variant["values"][2]["es"] : ""
            ]);
        }
    }







    /*
        Genera un string de la categoría de un producto a partir del array de categorias de un producto de tiendanube. (Sólo de la primera parent category)
        $categories: array de $productData["categories"]
        Devuelve string. Ejemplo: Accesorios/Bolsos/Mochilas
    */
    private function parseCategories($categories)
    {
        
        if(sizeof($categories) == 0) return "";

        for ($i=0; $i < sizeof($categories); $i++) 
        { 
            $category = $categories[$i];
            
            if($i == 0)
            {
                $parentCatId = $category["id"];
                $catString = $category["name"]["es"]." / ";
            }
            else
            {

                if($categories[$i]["parent"] == $parentCatId)
                {
                    $parentCatId = $category["id"];
                    $catString .= $category["name"]["es"]." / ";
                }
                else
                    break;

            }

        }

        return substr($catString, 0, -3);

    }





	/*
		Asignar a un producto de tiendanube, una categoría, teniendo el ID de la misma.
	*/
	public function assignCategoryToProduct($productId, $categoryId)
	{

		if(!$product = $this->api->GetProduct($productId, "fields=categories"))
			return false;

		$category_ids = $this->parseCategoryIds($product["categories"]);
		$category_ids[] = $categoryId;

		$data = ["categories" => $category_ids];

		if($this->api->EditProduct($productId, $data))
			return true;
		else
			return false;


	}


    /*
        Crea un array con solamente los ID's de categoría de un producto de tnube dado el array de datos de la api.
    */
    private function parseCategoryIds($categories)
    {
        $category_ids = [];

        foreach ($categories as $category) 
        {
            $category_ids[] =  $category["id"];
        }

        return $category_ids;
    }



    public function createCustomerFromApiData($customer)
    {

        $newCustomer = Customer::create([

            "id_customer_tnube" => $customer["id"],
            "nombre" => $customer["name"],
            "email" => $customer["email"],
            "telefono" => $customer["phone"],
            "total_gastado" => floatval($customer["total_spent"]),
            "fecha_registro" => date("Y-m-d H:i:s", strtotime($customer["created_at"])),
            "activo" => $customer["active"]

        ]);

        if($customer["default_address"] != null)
        {

            $newCustomer["direccion"] = $customer["default_address"]["address"] . " " . $customer["default_address"]["number"];
            $newCustomer["localidad"] = $customer["default_address"]["locality"];
            $newCustomer["ciudad"] = $customer["default_address"]["city"];
            $newCustomer["codigo_postal"] = $customer["default_address"]["zipcode"];
            $newCustomer["provincia"] = $customer["default_address"]["province"];
            $newCustomer["pais"] = $customer["default_address"]["country"];

        }

        $newCustomer->save();
    }


    public function setProductTags($productid, $tags)
    {
        if($this->api->EditProduct($productid, ["tags" => $tags]))
            return true;
        else
            return false;
    }



    /*
        Metodo para registrar nuevos clientes de tiendanube en la app.
        Devuelve el numero de clientes nuevos, o FALSE si hay error.
    */
    public function checkNewCustomers()
    {
        
        $lastCustomerId = Customer::select("id_customer_tnube")->orderBy("id_customer_tnube", "DESC")->first();

        if($lastCustomerId == null) 
            $lastCustomerId = 0;
        else
            $lastCustomerId = intval($lastCustomerId->id_customer_tnube);


        if($newCustomers = $this->api->GetCustomers("since_id=" . ($lastCustomerId + 1)))
        {

            foreach($newCustomers as $customer) {
                $this->createCustomerFromApiData($customer);
            }

            return sizeof($newCustomers);

        }
        else
        {

            if($this->api->http_response_no == 404) // 404 not found, no hay clientes nuevos.
                return 0;
            else
            {
                $this->error = $this->api->error;
                return false; 
            }
            
        } 

    }

    
}