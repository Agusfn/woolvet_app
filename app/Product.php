<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $guarded = [];


    public function brand()
    {
    	return $this->belongsTo(User::class, "user_id");
    }

    public function variants() 
    {
    	return $this->hasMany(ProductVariant::class);
    }

    public function sales()
    {
        return $this->hasMany(Sale::class);
    }


    /*
    	Metodo para obtener los productos que no tienen marca asignada
    */
    public static function ScopeUnassignedBrand($query)
    {
        return $query->where("user_id", "=", -1)->get();
    }




    public static function WithTnubeId($tNubeId)
    {
        return self::where("id_producto_tnube", $tNubeId);
    }





}
