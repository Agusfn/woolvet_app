<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $guarded = [];

	protected $dates = ['fecha_registro'];




    public function sales()
    {
        return $this->hasMany(Sale::class);
    }


    /*
        Método para obtener el ID de un cliente dado el ID de tiendanube asociado. Si no existe devuelve Null.
    */
    public static function GetIdFromTnubeId($tNubeId)
    {
        $customer = self::select("id")->where("id_customer_tnube", $tNubeId)->first();
        
        if($customer == null)
            return null;
        else
            return $customer->id;
    }



}
