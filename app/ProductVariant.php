<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductVariant extends Model
{
    protected $guarded = [];


    public function product()
    {
    	return $this->belongsTo(Product::class);
    }


    public static function WithTnubeId($tNubeId)
    {
        return self::where("id_variante_tnube", $tNubeId);
    }


}
